package fr.afpa.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity
public class Adresse {
	
	@Id
	private int idAd;
	@Column
	private int numero;
	@Column
	private String rue;
	@Column
	private String ville;
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name="idper")
	private Personne personne;
	
	
	public Adresse() {
		super();
	}
	
}
