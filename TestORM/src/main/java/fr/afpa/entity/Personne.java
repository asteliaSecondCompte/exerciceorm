package fr.afpa.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity
public class Personne {

	@Id
	private int idPer;
	@Column
	private String nom;
	@Column
	private String prenom;
	
	@Column
	@OneToMany(mappedBy = "personne")
	private List<Adresse> adresses;
	
	@OneToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name="id_compte")
	private Compte compte;
	
	public Personne() {
		super();
	}
	
}
