package fr.afpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity
public class Compte {
	@Id
	private int id_compte;
	@Column
	private String login;
	@Column
	private String mdp;
	
	@OneToOne(mappedBy = "compte")
	private Personne personne;
	
	public Compte() {
		super();
	}
	
	

}
