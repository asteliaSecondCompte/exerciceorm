package fr.afpa.main;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.entity.Adresse;
import fr.afpa.entity.Compte;
import fr.afpa.entity.Personne;
import fr.afpa.utils.HibernateUtils;

public class Main {
	
	private static Session session;
	
	public static void main(String[] args) {
		
		session = HibernateUtils.getSession();
		
		Compte compte1 = new Compte();
		compte1.setId_compte(1);
		compte1.setLogin("Batman");
		compte1.setMdp("batarang");
		
		Compte compte2 = new Compte();
		compte2.setId_compte(2);
		compte2.setLogin("LeMaitre");
		compte2.setMdp("lapomme");
		
		Adresse adresse1 = new Adresse();
		adresse1.setIdAd(1);
		adresse1.setNumero(4);
		adresse1.setRue("Manoir Wayne");
		adresse1.setVille("Gotham City");
		
		Adresse adresse2 = new Adresse();
		adresse2.setIdAd(2);
		adresse2.setNumero(2);
		adresse2.setRue("Lapomme");
		adresse2.setVille("Florence");
		
		
		Personne personne1 = new Personne();
		personne1.setIdPer(1);
		personne1.setNom("WAYNE");
		personne1.setPrenom("Bruce");
		personne1.setCompte(compte1);
		
		Personne personne2 = new Personne();
		personne2.setIdPer(2);
		personne2.setNom("AUDITORE");
		personne2.setPrenom("Ezio");
		personne2.setCompte(compte2);
		
		adresse1.setPersonne(personne1);
		adresse2.setPersonne(personne2);
		
		Transaction transaction = session.beginTransaction();
		
		session.save(adresse1);
		session.save(adresse2);
		session.save(compte1);
		session.save(compte2);
		session.save(personne1);
		session.save(personne2);
		
		transaction.commit();
		session.close();
		
	}
	
}
