/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Exemple 1
function declarationVariables() {
    var maVariable;             // Son type est undefined
    console.log(maVariable);
    maVariable = 324;           // Son type devient number (base 10)
    console.log(maVariable);
    maVariable = 0324;          // Son type reste number (base 8)
    console.log(maVariable);
    maVariable = 0x324;         // Son type reste number (base 16)
    console.log(maVariable);
    maVariable = "Bonjour";     // Son type devient string
    console.log(maVariable);
    maVariable = true;          // Son type devient boolean
    console.log(maVariable);
    maVariable = new Array();   // Son type devient Object ou Array
    console.log(maVariable);
}

// Exemple 2
function tableauDixValeurs() {
    var tabScore = new Array(10);
    console.log(tabScore);
    tabScore[1] = 24;
    console.log(tabScore);
}

// Exemple 3
function operateurIdentite() {
    console.log("42 == '42' retourne " + (42 == '42')); // Retourne true
    console.log("0 == false retourne " + (0 == false)); // Retourne true
    console.log("42 === '42' retourne " + (42 === "42")); // Retourne false
    console.log("0 === false retourne " + (0 === false)); // Retourne false
}

// Exemple 4
function conditions() {
    var genre = "f";
    if (genre == "h") {
        console.log("test if : Monsieur");
    } else {
        console.log("test if : Madame");
    }

    console.log((genre == "h") ? "test ternaire : Monsieur" : "test ternaire : Madame");
}

// Exemple 5
function conditionswitch() {
    var animalTeste = document.getElementById("animal").value;
    switch (animalTeste) {
        case "chien" :
            ;
        case "oiseau" :
            ;
        case "poisson" :
            ;
        case "vache" :
            console.log(animalTeste + " : C'est un vertébré");
            break;
        case "mouche" :
            ;
        case "libellule" :
            ;
        case "papillon" :
            console.log(animalTeste + " : C'est un invertébré");
            break;
        default :
            console.log(animalTeste + " : Ce n'est pas un animal");
    }
}

// Exemple 6
function boucleFor() {
    var nb = document.getElementById("nbBoucles").value;
    if (!isNaN(nb)) {
        for (var i = 0; i < nb; i++) {
            console.log(i);
        }
    }
    else {
        console.log("Ce n'est pas un nombre.");
    }
}