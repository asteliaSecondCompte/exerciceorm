package fr.afpa.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import fr.afpa.dao.DAOPersonne;
import fr.afpa.entity.AdresseBDD;
import fr.afpa.entity.CompteBDD;
import fr.afpa.entity.PersonneBDD;
import fr.afpa.objets.Adresse;
import fr.afpa.objets.Compte;
import fr.afpa.objets.Utilisateur;

public class DTOPersonne {
	
	/**
	 * Permet d'ajouter une adresse a l'utilisateur
	 * @param adresseMetier : l'adresse a ajouter a l'utilisateur
	 * @param idUser : l'id de l'utilisateur
	 * @param utilisateur : l'utilisateur a qui on ajoute l'adresse
	 * @return 
	 */
	public boolean creerAdresse(Adresse adresseMetier, int idUser, Utilisateur utilisateur) {
		AdresseBDD adresseBDD = adresseToAdresseBDD(adresseMetier);
		PersonneBDD personne = userToPersonne(idUser, utilisateur);
		
		personne.getAdresses().add(adresseBDD);
		adresseBDD.setPersonne(personne);
		
		DAOPersonne daop = new DAOPersonne();
		return daop.ajouterAdresse(personne, adresseBDD);
	}
	
	/**
	 * Permet de creer une personne a partir d'un utilisateur
	 * @param utilisateur : l'utilisateur a transformer
	 * @return
	 */
	public boolean creerPersonne(Utilisateur utilisateur) {
		PersonneBDD personne = userToPersonne(utilisateur);
		CompteBDD compte = userToCompteBDD(utilisateur);
		
		personne.setCompte(compte);
		compte.setPersonne(personne);
		
		DAOPersonne daop = new DAOPersonne();
		return daop.ajoutPersonne(personne, compte);
	}
	
	/**
	 * Permet de retourner l'utilisateur qui correspond a l'id passe en parametre
	 * @param id : l'id de l'utilisateur recherche
	 * @return une liste contenant l'utilisateur s'il a ete trouve et une liste
	 * vide sinon
	 */
	public Map<Integer, Utilisateur> utilisateurParId(int id) {
		Map<Integer, Utilisateur> res = new HashMap<Integer, Utilisateur>();
		DAOPersonne daop = new DAOPersonne();
		List<PersonneBDD> listePersonnes = daop.personneParId(id);
		for (PersonneBDD personne : listePersonnes) {
			res.putAll(personneToUser(personne));
		}
		return res;
	}
	
	/**
	 * Permet de recuperer la liste de tous les utilisateurs
	 * @return la liste de tous les utilisateurs
	 */
	public Map<Integer, Utilisateur> listeUtilisateurs() {
		Map<Integer, Utilisateur> res = new HashMap<Integer, Utilisateur>();
		DAOPersonne daop = new DAOPersonne();
		List<PersonneBDD> listePersonnes = daop.listePersonnes();
		for (PersonneBDD personne : listePersonnes) {
			res.putAll(personneToUser(personne));
		}
		return res;
	}
	
	/**
	 * Permet de recuperer la liste des adresses d'un utilisateur a partir de son id
	 * @param idUser : l'id de l'utilisateur
	 * @return la liste de toutes les adresses d'un utilisateur dont l'id est passe en parametre
	 * sinon une liste vide
	 */
	public List<Adresse> listeAdresses(int idUser) {
		List<Adresse> res = new ArrayList<Adresse>();
		DAOPersonne daop = new DAOPersonne();
		for (AdresseBDD adrBDD : daop.listeAdresses(idUser)) {
			res.add(adresseBDDToAdresse(adrBDD));
		}
		return res;
	}
	
	/**
	 * Permet de modifier les donnees d'un utilisatuer
	 * (nom, prenom, mot de passe)
	 * @param id : l'id de l'utilisateur a modifier
	 * @param utilisateur : l'utilisateur a modifier
	 * @return
	 */
	public boolean modifierPersonne(int id, Utilisateur utilisateur) {
		PersonneBDD personne = userToPersonne(id, utilisateur);
		CompteBDD compte = userToCompteBDD(utilisateur);
		compte.setId_compte(utilisateur.getCompte().getId());
		personne.setCompte(compte);
		
		DAOPersonne daop = new DAOPersonne();
		return daop.modifierPersonne(personne, compte);
	}
	
	/**
	 * Permet de supprimer un utilisateur
	 * @param id : l'id de l'utlisateur a supprimer
	 * @param utilisateur : l'utilisateur a supprimer
	 * @return
	 */
	public boolean supprimerPersonne(int id, Utilisateur utilisateur) {
		PersonneBDD personne = userToPersonne(id, utilisateur);
		CompteBDD compte = userToCompteBDD(utilisateur);
		compte.setId_compte(utilisateur.getCompte().getId());
		compte.setPersonne(personne);
		personne.setCompte(compte);
		
		DAOPersonne daop = new DAOPersonne();
		return daop.supprimerPersonne(compte);
	}
	
	/**
	 * Permet de recuperer l'utilisateur ui s'authentifie
	 * @param login : le login de l'utilisateur
	 * @param mdp : le mot de passe de l'utilisateur
	 * @return une liste contenant l'utilisateur s'il a ete trouve et une liste
	 * vide sinon
	 */
	public Map<Integer, Utilisateur> authentification(String login, String mdp) {
		Map<Integer, Utilisateur> res = new HashMap<Integer, Utilisateur>();
		DAOPersonne daop = new DAOPersonne();
		List<PersonneBDD> listePersonnes = daop.loginMdp(login, mdp);
		for (PersonneBDD personne : listePersonnes) {
			res.putAll(personneToUser(personne));
		}
		return res;
	}
	
	/**
	 * Permet de transformer une personne en utilisateur
	 * @param personne : la personne a transformer
	 * @return une liste contenant la personne transformee en utilisateur
	 */
	private Map<Integer, Utilisateur> personneToUser(PersonneBDD personne) {
		Map<Integer, Utilisateur> res = new HashMap<Integer, Utilisateur>();
		Utilisateur user = new Utilisateur(personne.getNom(), personne.getPrenom(), compteBDDToCompte(personne.getCompte()));
		res.put(personne.getIdPer(), user);
		return res;
	}
	
	/**
	 * Permet de transfermer un compte de base de donnees en un compte metier
	 * @param cptBDD : le compte de base de donnees a transformer
	 * @return le compte de base de donnees transforme en compte metier
	 */
	private Compte compteBDDToCompte(CompteBDD cptBDD) {
		Compte compte = new Compte(cptBDD.getLogin(), cptBDD.getMdp());
		compte.setId(cptBDD.getId_compte());
		return compte;
	}
	
	/**
	 * Permet de transformer un utilisateur en personne 
	 * @param user : l'utilisateur a transformer
	 * @return l'utilisateur transforme en personne
	 */
	private PersonneBDD userToPersonne(Utilisateur user) {
		PersonneBDD personne = new PersonneBDD();
		personne.setNom(user.getNom());
		personne.setPrenom(user.getPrenom());
		return personne;
	}
	
	/**
	 * Permet de transformer un utilisateur et un id en personne 
	 * @param id : l'id a transformer
	 * @param user : l'utilisateur a transformer
	 * @return l'utilisateur et l'id transformes en personne
	 */
	private PersonneBDD userToPersonne(int id, Utilisateur user) {
		PersonneBDD personne = new PersonneBDD();
		personne.setNom(user.getNom());
		personne.setPrenom(user.getPrenom());
		personne.setIdPer(id);
		return personne;
	}
	
	/**
	 * Permet de creer un compte de base de donnees a partir des 
	 * donnees d'un utilisateur
	 * @param utilisateur : utilisateur a transformer
	 * @return un compte de base de donnees
	 */
	private CompteBDD userToCompteBDD(Utilisateur utilisateur) {
		CompteBDD compte = new CompteBDD();
		compte.setLogin(utilisateur.getCompte().getLogin());
		compte.setMdp(utilisateur.getCompte().getMdp());
		return compte;
	}
	
	/**
	 * Permet de transformer une adresse de base de donnees en adresse metier
	 * @param adrBDD : adresse a transformer
	 * @return une adresse metier
	 */
	private Adresse adresseBDDToAdresse(AdresseBDD adrBDD) {
		Adresse adresse = new Adresse();
		adresse.setNumero(adrBDD.getNumero());
		adresse.setRue(adrBDD.getRue());
		adresse.setVille(adrBDD.getVille());
		return adresse;
	}
	
	/**
	 * Permet de transformer une adresse metier en adresse de base de donnees
	 * @param adresse : l'adresse a transformer
	 * @return une adresse de base de donnees
	 */
	private AdresseBDD adresseToAdresseBDD(Adresse adresse) {
		AdresseBDD adresseBDD = new AdresseBDD();
		adresseBDD.setNumero(adresse.getNumero());
		adresseBDD.setRue(adresse.getRue());
		adresseBDD.setVille(adresse.getVille());
		return adresseBDD;
	}
	
}
