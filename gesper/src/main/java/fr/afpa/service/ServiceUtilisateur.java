package fr.afpa.service;

import java.util.Map;
import java.util.Map.Entry;

import fr.afpa.dto.DTOPersonne;
import fr.afpa.objets.Adresse;
import fr.afpa.objets.Compte;
import fr.afpa.objets.Utilisateur;

public class ServiceUtilisateur {
	
	public static final String NOM = "nom";
	public static final String PRENOM = "prenom";
	public static final String MDP = "mdp";
	
	
	/**
	 * Permet de creer une adresse et de l'ajouter a un utilisateur
	 * @param numero : le numero de la rue
	 * @param rue : le nom de la rue
	 * @param ville : le nom de la ville
	 * @param idUser : l'id de l'utilisateur
	 * @param utilisateur : l'utilisateur auquel on ajoute l'adresse
	 * @return
	 */
	public boolean creerAdresse(int numero, String rue, String ville, int idUser, Utilisateur utilisateur) {
		Adresse adresse = new Adresse(numero, rue, ville);
		DTOPersonne dtop = new DTOPersonne();
		return dtop.creerAdresse(adresse, idUser, utilisateur);
	}
	
	/**
	 * Permet de creer un utilisateur et ses identifiants
	 * @param nom : le nom de l'utilisateur
	 * @param prenom : le prenom de l'utilisateur
	 * @param login : le login de l'utilisateur
	 * @param mdp : le mot de passe de l'utilisateur
	 * @return
	 */
	public boolean creerUtilisateur(String nom, String prenom, String login, String mdp) {
		Utilisateur utilisateur = new Utilisateur(nom, prenom, new Compte(login, mdp));
		DTOPersonne dtop = new DTOPersonne();
		return dtop.creerPersonne(utilisateur);
	}
	
	/**
	 * Permet de retourner l'utilisateur rechercher via son id
	 * @param id : id de l'utilisateur recherche
	 * @return une liste contenant l'utilisateur s'il a ete trouve sinon
	 * une liste vide
	 */
	public Map<Integer, Utilisateur> rechercheUtilisateurParId(int id) {
		DTOPersonne dtop = new DTOPersonne();
		return dtop.utilisateurParId(id);
	}
	
	/**
	 * Permet de retourner la liste de tous les utilisateurs
	 * @return la liste de tous les utilisateurs
	 */
	public Map<Integer, Utilisateur> consulterUtilisateurs() {
		DTOPersonne dtop = new DTOPersonne();
		return dtop.listeUtilisateurs();
	}
	
	/**
	 * Permet de retourner la liste des adresses d'un utilisateur via son id
	 * @param idUser : l'id de l'utilisateur
	 * @return une chaine de caracteres representant la liste des adresses d'un utilisateur
	 */
	public String rechercherListeAdresses(int idUser) {
		String res = "";
		DTOPersonne dtop = new DTOPersonne();
		for (Adresse adresse : dtop.listeAdresses(idUser)) {
			res += adresse.getNumero()+" "+adresse.getRue()+", "+adresse.getVille()+"\n";
		}
		return res;
	}
	
	/**
	 * Permet de modifier l'une des informations d'un utilisateur
	 * @param id : l'id de l'utilisateur a modifier
	 * @param utilisateur : l'utilisateur a modifier
	 * @param typeInfo : le type de l'info a modifier (constantes de ServiceUtilisateur)
	 * @param nouvInfo : la ouvelle information
	 * @return
	 */
	public boolean modifierUtilisateur(int id, Utilisateur utilisateur, String typeInfo, String nouvInfo) {
		switch (typeInfo) {
			case NOM : utilisateur.setNom(nouvInfo);
						break;
			case PRENOM : utilisateur.setPrenom(nouvInfo);
						break;
			case MDP : utilisateur.getCompte().setMdp(nouvInfo);
						break;
			default: break;
		}
		DTOPersonne dtop = new DTOPersonne();
		return dtop.modifierPersonne(id, utilisateur);
	}
	
	/**
	 * Permet de supprimer un utilisateur
	 * @param id : l'id de l'utilisateur a supprimer
	 * @param utilisateur : l'utilisateur a supprimer
	 * @return
	 */
	public boolean supprimerUtilisateur(int id, Utilisateur utilisateur) {
		DTOPersonne dtop = new DTOPersonne();
		return dtop.supprimerPersonne(id, utilisateur);
	}
	
	/**
	 * Permet de s'authentifier
	 * @param login : le login de l'utilisateur
	 * @param mdp : le mot de passe de l'utilisateur
	 * @return
	 */
	public Map<Integer, Utilisateur> authentification(String login, String mdp) {
		DTOPersonne dtop = new DTOPersonne();
		return dtop.authentification(login, mdp);
	}

	/**
	 * Permet de transformer la liste des utilisateurs en un tableau a deux dimensions
	 * pour l'afficher dans la table de l'ihm
	 * @param titres : le nom des colonnes
	 * @param listeUsers : la liste des utilisateurs
	 * @return
	 */
	public String[][] listeToTableau(String[] titres, Map<Integer, Utilisateur> listeUsers) {
		String[][] res = new String[listeUsers.size()][titres.length];
		int i=0;
		for (Entry<Integer, Utilisateur> utilisateur : listeUsers.entrySet()) {
			Utilisateur user = utilisateur.getValue();
			res[i] = new String[] {""+utilisateur.getKey()
						, user.getNom(), user.getPrenom()
						, user.getCompte().getLogin()
						, user.getCompte().getMdp()};
			
			i++;
		}
		return res;
	}
	
}
