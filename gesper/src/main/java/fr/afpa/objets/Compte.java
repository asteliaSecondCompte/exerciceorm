package fr.afpa.objets;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Compte {
	
	private String login;
	private String mdp;
	private int id;
	
	
	public Compte() {
		super();
	}

	public Compte(String login, String mdp) {
		super();
		this.login = login;
		this.mdp = mdp;
	}
	

}
