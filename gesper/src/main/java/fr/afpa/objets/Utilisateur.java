package fr.afpa.objets;

import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Utilisateur {
	
	private String nom;
	private String prenom;
	private Compte compte;
	private List<Adresse> listeAdresses;
	
	
	public Utilisateur() {
		super();
	}

	public Utilisateur(String nom, String prenom, Compte compte, List<Adresse> listeAdresses) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.compte = compte;
		this.listeAdresses = listeAdresses;
	}
	
	public Utilisateur(String nom, String prenom, Compte compte) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.compte = compte;
		this.listeAdresses = new ArrayList<Adresse>();
	}

	
	public boolean addAdresse(Adresse adresse) {
		return this.listeAdresses.add(adresse);
	}
	
	public boolean removeAdresse(Adresse adresse) {
		return this.listeAdresses.remove(adresse);
	}
	
	public Adresse getAdresse(int ind) {
		if (0<= ind && ind <this.listeAdresses.size()) {
			return this.listeAdresses.get(ind);
		}
		else {
			return null;
		}
	}
	
	public boolean contains(Adresse adresse) {
		return this.listeAdresses.contains(adresse);
	}
	
}
