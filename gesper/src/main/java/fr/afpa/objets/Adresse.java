package fr.afpa.objets;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Adresse {

	private int numero;
	private String rue;
	private String ville;
	
	
	public Adresse() {
		super();
	}

	public Adresse(int numero, String rue, String ville) {
		super();
		this.numero = numero;
		this.rue = rue;
		this.ville = ville;
	}
	
	
}
