package fr.afpa.ihm;

import javax.swing.table.DefaultTableModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserTableModel extends DefaultTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public UserTableModel() {
		super();
	}
	
	public UserTableModel(String[][] tableau, String[] titres) {
		super(tableau, titres);
	}

}
