package fr.afpa.dao;

import java.util.ArrayList;
import java.util.List;


import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.afpa.entity.AdresseBDD;
import fr.afpa.entity.CompteBDD;
import fr.afpa.entity.PersonneBDD;
import fr.afpa.utils.HibernateUtils;

public class DAOPersonne {

	/**
	 * Permet d'ajouter une adresse a une personne dans la base de donnees
	 * @param personne : la personne a qui on ajoute l'adresse
	 * @param adresse : l'adresse a ajouter
	 * @return 
	 */
	public boolean ajouterAdresse(PersonneBDD personne, AdresseBDD adresse) {
		Session session = HibernateUtils.getSession();
		Transaction transaction = session.beginTransaction();
		
		session.save(adresse);
		session.update(personne);
		
		transaction.commit();
		session.close();
		return true;
	}
	
	/**
	 * Permet d'ajouter une personne et ses identifiants a la base de donnees
	 * @param personne : la personne a ajouter
	 * @param compte : le compte a ajouter
	 * @return
	 */
	public boolean ajoutPersonne(PersonneBDD personne, CompteBDD compte) {
		Session session = HibernateUtils.getSession();
		Transaction transaction = session.beginTransaction();
		
		session.save(personne);
		session.save(compte);
		
		transaction.commit();
		session.close();
		return true;
	}
	
	/**
	 * Permet de recuperer la personne qui correspond a l'id passe en parametre
	 * @param id : l'id de la personne recherchee
	 * @return une liste contenant la personne recherchee si elle est trouvee
	 * sinon une liste vide
	 */
	public List<PersonneBDD> personneParId(int id) {
		Session session = HibernateUtils.getSession();
		Query<PersonneBDD> query = session.createQuery("from personne where idper="+id);
		List<PersonneBDD> listePersonnes = query.list();
		session.close();
		return listePersonnes;
	}
	
	/**
	 * Permet de recuperer la liste des personnes dans la base de donnees
	 * @return la liste de toutes les personnes de la base de donnees
	 */
	public List<PersonneBDD> listePersonnes() {
		Session session = HibernateUtils.getSession();
		Query<PersonneBDD> query = session.createQuery("from personne");
		List<PersonneBDD> listePersonnes = query.list();
		session.close();
		return listePersonnes;
	}
	
	/**
	 * Permet de retourner le compte associe a l'id personne
	 * @param id :
	 * @return une liste contenant le compte recherche s'il est trouve
	 * sinon une liste vide
	 */
	public List<CompteBDD> compteRecherche(int id) {
		Session session = HibernateUtils.getSession();
		Query<CompteBDD> query = session.createQuery("from compte where id_compte="+id);
		List<CompteBDD> listeComptes = query.list();
		session.close();
		return listeComptes;
	}
	
	/**
	 * Permet de retourner la liste des adresses de la personne dont l'id est passe en parametre
	 * @param idUser : l'id de la personne qui possede les adresses
	 * @return la liste des adressses de  la personne dont l'id est passe en parametre
	 */
	public List<AdresseBDD> listeAdresses(int idUser) {
		Session session = HibernateUtils.getSession();
		Query<AdresseBDD> query = session.createQuery("from adresse where idper="+idUser);
		List<AdresseBDD> listeAdresses = query.list();
		session.close();
		return listeAdresses;
	}
	
	/**
	 * Permet de mettre a jour une personne et son compte avec ses nouvelles informations
	 * @param personne : personne a mettre a jour
	 * @param compte : compte de la personne a mettre a jour
	 * @return
	 */
	public boolean modifierPersonne(PersonneBDD personne, CompteBDD compte) {
		Session session = HibernateUtils.getSession();
		Transaction transaction = session.beginTransaction();
		
		session.update(compte);
		session.update(personne);
		
		transaction.commit();
		session.close();
		return true;
	}
	
	/**
	 * Permet de supprimer une personne de la base de donnees
	 * @param personne : la personne a supprimer
	 * @return
	 */
	public boolean supprimerPersonne(CompteBDD compte) {//() {
		Session session = HibernateUtils.getSession();
		Transaction transaction = session.beginTransaction();
		
		session.delete(compte);
		
		transaction.commit();
		session.close();
		return true;
	}
	
	/**
	 * Permet de retourner la personne qui correspond au login et au mot de passe
	 * passes en parametre
	 * @param login : le login
	 * @param mdp : le mot de passe
	 * @return une liste contenant la personne recherchee si elle a ete trouvee
	 * sinon une liste vide
	 */
	public List<PersonneBDD> loginMdp(String login, String mdp) {
		Session session = HibernateUtils.getSession();
		Query<PersonneBDD> query = session.createQuery("from personne where id_compte= (select id_compte from compte where login='"+login+"' and mdp='"+mdp+"')");
		List<PersonneBDD> listePersonnes = query.list();
		session.close();
		return listePersonnes;
	}
	
}
