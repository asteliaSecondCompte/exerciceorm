package fr.afpa.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity(name = "compte")
public class CompteBDD {
	@Id
	@OnDelete(action = OnDeleteAction.CASCADE)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_compte;
	@Column
	private String login;
	@Column
	private String mdp;
	
	@OneToOne(mappedBy = "compte", cascade = {CascadeType.ALL})
	private PersonneBDD personne;
	
	public CompteBDD() {
		super();
	}
	
	

}
