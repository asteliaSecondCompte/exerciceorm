package fr.afpa.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity(name = "personne")
public class PersonneBDD {

	@Id
	@OnDelete(action = OnDeleteAction.CASCADE)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idPer;
	@Column
	private String nom;
	@Column
	private String prenom;
	
	@Column
	@OneToMany(mappedBy = "personne")
	private List<AdresseBDD> adresses;
	
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="id_compte")
	private CompteBDD compte;
	
	public PersonneBDD() {
		super();
		this.adresses = new ArrayList<AdresseBDD>();
		this.compte = new CompteBDD();
	}
	
}
