package fr.afpa.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity(name = "adresse")
public class AdresseBDD {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idAd;
	@Column
	private int numero;
	@Column
	private String rue;
	@Column
	private String ville;
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="idper")
	private PersonneBDD personne;
	
	
	public AdresseBDD() {
		super();
	}
	
}
