package fr.afpa.controle;

import fr.afpa.service.ServiceUtilisateur;

public class ControleUtilisateur {

	/**
	 * Permet de verifier si le login et le mot de passe sont dans
	 * la ba se de donnees
	 * @param login : login a verifier
	 * @param mdp : mot de passe a verifier
	 * @return true si le login et le mot de passe sont dans la base de donnees
	 * et false sinon
	 */
	public boolean authentifReussie(String login, String mdp) {
		ServiceUtilisateur su = new ServiceUtilisateur();
		return !su.authentification(login, mdp).isEmpty();
	}
	
	/**
	 * Permet de verifier si l'id passe en parametre correspond a
	 * un utilisateur dans la base de donnees
	 * @param id : l'id a verifier
	 * @return true si l'id correspond a un utilisateur dans la base de donnees
	 */
	public boolean idCorrect(int id) {
		ServiceUtilisateur su = new ServiceUtilisateur();
		return !su.rechercheUtilisateurParId(id).isEmpty();
	}
	
}
