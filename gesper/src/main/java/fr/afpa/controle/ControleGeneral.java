package fr.afpa.controle;

public class ControleGeneral {

	/**
	 * Permet de verifier si le String passe en parametre ne contient que des lettres
	 * @param nom : nom ou prenom
	 * @return true si le String est valide et false sinon
	 */
	public static boolean nomPrenomValide(String nom) {
		return nom.matches("[A-Z\\-]+");
	}
	
	/**
	 * Permet de verifier si le String passe en parametre ne contient que des lettres
	 * et/ou des chiffres
	 * @param login : login ou mot de passe
	 * @return true si le String est valide et false sinon
	 */
	public static boolean loginMdpValide(String login) {
		return login.matches("[a-zA-Z0-9]+");
	}
	
	/**
	 * Permet de verifier si le String passe en parametre represente un entier
	 * @param numero : numero a verifier
	 * @return true si l'entier est valide et false sinon
	 */
	public static boolean entierValide(String numero) {
		try {
			return Integer.parseInt(numero) > 0;
		} catch (NumberFormatException ne) {
			return false;
		}
	}
	
	/**
	 * Permet de verifier si le nom de rue passe en parametre est valide
	 * @param rue : le nom de la rue a verifier
	 * @return true si le nom de la rue est valide et false sinon
	 */
	public static boolean nomRueVilleValide(String rue) {
		return rue.matches("[a-zA-Z\\s\\-]+");
	}
	
}
