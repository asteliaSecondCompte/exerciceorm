-- Drop table
/*
DROP TABLE public.adresse;
DROP TABLE public.personne;
DROP TABLE public.compte;
*/

-- Creation des tables

CREATE TABLE public.compte (
	id_compte int4 NOT NULL,
	login varchar(255) NULL,
	mdp varchar(255) NULL,
	CONSTRAINT pk_compte PRIMARY KEY (id_compte)
);

CREATE TABLE public.personne (
	idper int4 NOT NULL,
	nom varchar(255) NULL,
	prenom varchar(255) NULL,
	id_compte int4 NULL,
	CONSTRAINT pk_personne PRIMARY KEY (idper),
	CONSTRAINT fk_personne_compte FOREIGN KEY (id_compte) REFERENCES compte(id_compte) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE public.adresse (
	idad int4 NOT NULL,
	numero int4 NULL,
	rue varchar(255) NULL,
	ville varchar(255) NULL,
	idper int4 NULL,
	CONSTRAINT pk_adresse PRIMARY KEY (idad),
	CONSTRAINT fk_adresse_personne FOREIGN KEY (idper) REFERENCES personne(idper) ON DELETE CASCADE ON UPDATE CASCADE
);


-- Permissions

ALTER TABLE public.compte OWNER TO postgres;
GRANT ALL ON TABLE public.compte TO postgres;

ALTER TABLE public.personne OWNER TO postgres;
GRANT ALL ON TABLE public.personne TO postgres;

ALTER TABLE public.adresse OWNER TO postgres;
GRANT ALL ON TABLE public.adresse TO postgres;


-- Remplissage tables
INSERT INTO public.compte (id_compte, login, mdp) VALUES(1, 'admin', 'admin');
INSERT INTO public.personne (idper, nom, prenom, id_compte) VALUES(1, 'WAYNE', 'BRUCE', 1);
INSERT INTO public.adresse (idad, numero, rue, ville, idper) VALUES(1, 4, 'Manoir Wayne', 'Gotham City', 1);
