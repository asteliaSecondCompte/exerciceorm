﻿Projet Gesper Astelia


0 - Avant utilisation

	- Créer une base de données vide nommée "gesper" dont le mot de passe est "admin".
	- A partir du fichier "ConstructionBDD.sql" créer les tables et les remplir.



1 - Authentification

	Pour s'authentifier, le login et le mot de passe par défaut sont "admin", "admin".
On vous souhaite la bienvenue avec votre nom et votre prénom.



2 - Lister utilisateurs

	Tous les utilisateurs s'affichent dans la table "Liste des utilisateurs".
Elle est mise à jour automatiquement.



3 - Création utilisateur

	Dans la zone "Créer / Modifier / Supprimer" :
	Remplir tous les champs sauf "ID de l'utilisateur",
puis appuyer sur le bouton "Créer".
Le nouvel utilisateur apparaît dans la table "Liste des utilisateurs".



4 - Modification utilisateur

	Dans la zone "Créer / Modifier / Supprimer" :
	Entrer l'Id d'un utilisateur, présent dans la table "Liste des utilisateurs", dans le champ 
"ID de l'utilisateur". Puis, remplir le champ que vous souhaîtez modifier et appuyer sur le 
bouton "modifier" associé (seuls les champs à côté d'un bouton modifier peuvent être changer).
La modification est visible dans la table "Liste des utilisateurs".



5 - Supprimer utilisateur

	Dans la zone "Créer / Modifier / Supprimer" :
	Entrer l'Id d'un utilisateur, présent dans la table "Liste des utilisateurs", dans le champ 
"ID de l'utilisateur" puis, appuyer sur le bouton supprimer.
L'utilisateur devrait diparaître de la table "Liste des utilisateurs".



6 - Ajouter une adresse à un utilisateur

	Dans la zone "Ajouter une adresse à un utilisateur" :
	Remplir tous les champs puis appuyer sur ajouter.



7 - Afficher la liste d'adresses d'un utilisateur

	Dans la zone "Adresse(s) de l'utilisateur cherché" :
	Entré l'Id d'un utilisateur, présent dans la table "Liste des utilisateurs", dans le champ
"ID de l'utilisateur recherché", puis appuyer sur le bouton "Rechercher".



8 - Retour authentification

	Appuyer sur le bouton "< Retour" pour revenir à l'authentification.