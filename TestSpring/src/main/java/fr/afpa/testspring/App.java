package fr.afpa.testspring;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.afpa.conduite.Person;
import fr.afpa.entitesmetiers.Personne;
import fr.afpa.nations.European;

public class App {
	
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//Personne p = context.getBean("per", Personne.class);
		
		BeanFactory factory =  context;
		Personne p = (Personne) factory.getBean("per");
		
		p.afficher();
		
		
		European e = (European) context.getBean("french");
		e.saluer();
		
		e = (European) context.getBean("english");
		e.saluer();
		
		
		Person p2 = (Person) context.getBean("person");
		p2.drive();
	}
	
}
