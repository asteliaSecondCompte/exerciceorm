package fr.afpa.nations;

import org.springframework.stereotype.Component;

@Component
public interface European {

	public void saluer();
	
}
