package fr.afpa.nations;

import org.springframework.stereotype.Component;

@Component
public class French implements European {

	@Override
	public void saluer() {
		System.out.println("Bonjour");
	}

}
