package fr.afpa.conduite;

import org.springframework.stereotype.Component;

@Component
public class Person {

	Car c;
	
	public void drive() {
		System.out.println("Je suis prete a conduire");
		c.start();
	}
	
}
