package fr.afpa.entitesmetiers;

import java.util.List;


public class Personne {
	
	private int id;
	private String nom;
	private Adresse adresse;
	private List<String> sports;
	
	
	public Personne(int id, String nom, Adresse adresse, List<String> sports) {
		this.id = id;
		this.nom = nom;
		this.adresse = adresse;
		this.sports = sports;
	}
	
	
	public void afficher() {
		System.out.println(id + " " + nom + " " + adresse);
		System.out.println("Mes sports : ");
		sports.stream().forEach(System.out::println);
	}

}
