package fr.afpa.interfaces.services;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.afpa.entites.Reservation;
import fr.afpa.entites.Salle;

@Service
public interface IServiceModificationSalle {

	List<Salle> voirSalle();

	Salle getSalle(String id);

	Object listerBatiment();

	boolean updateSalle(Salle salle);

	boolean supprimerSalle(int parseInt);

	String voirMateriel(int id);

	String listeSalleComplete();

	Salle getSalleComplete(String id);
	
	/**
	 * Permet d'annuler une reservation a partir de son id
	 * @param id : id de la reservation
	 * @return true si la reservation a ete annulee et false sinon
	 */
	public boolean annulerReservation(int id);
	
	/**
	 * Permet de modifier une reservation
	 * @param reserv : reservation modifiee
	 * @param idSalle : id de la salle qui possede la reservation
	 * @return true si la modification a ete effectuee et false sinon
	 */
	public boolean modifierReservation(Reservation reserv, int idSalle);

}
