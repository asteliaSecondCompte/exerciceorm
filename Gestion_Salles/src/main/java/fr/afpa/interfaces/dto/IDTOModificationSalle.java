package fr.afpa.interfaces.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import fr.afpa.entites.Batiment;
import fr.afpa.entites.Reservation;
import fr.afpa.entites.Salle;

@Service
public interface IDTOModificationSalle {

	Salle choixSalle(String id);

	List<Salle> listeSalles();

	ArrayList<Batiment> listerBatiment();

	boolean updateSalle(Salle salle);

	boolean supprimerSalle(int parseInt);

	Map<String, Integer> voirMateriel(int id);
	
	public List<Salle> voirSalles();

	Salle choixSalleComplete(String id);
	
	/**
	 * Permet de supprimer une reservation via son id
	 * @param id : id de la reservation a supprimer
	 * @return true si la reservation a ete supprimee et false sinon
	 */
	public boolean supprimerReservation(int id);
	
	/**
	 * Permet d'effectuer la modification d'une reservation
	 * @param reserv : la reservation modifiee
	 * @param idSalle : l'id de la salle reservee
	 * @return true si la modification a ete effectuee et false sinon 
	 */
	public boolean modifierReservation(Reservation reserv, int idSalle);

}
