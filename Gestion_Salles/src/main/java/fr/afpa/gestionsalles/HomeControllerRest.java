package fr.afpa.gestionsalles;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import fr.afpa.entites.Message;
import fr.afpa.entites.ModifReserv;
import fr.afpa.entites.ModifSalle;
import fr.afpa.entites.ModifUser;
import fr.afpa.entites.Personne;
import fr.afpa.entites.Reservation;
import fr.afpa.entites.ReserverInfo;
import fr.afpa.entites.AjoutSalle;
import fr.afpa.entites.ChoixSalle;
import fr.afpa.entites.CreateUser;
import fr.afpa.entites.EnvoyerMessage;
import fr.afpa.entites.RolePersonne;
import fr.afpa.entites.Salle;
import fr.afpa.entites.TypeSalle;
import fr.afpa.entites.Utilisateur;
import fr.afpa.entites.VoirMessage;
import fr.afpa.interfaces.controles.IControleAuthentificationUtilisateur;
import fr.afpa.interfaces.controles.IControleChoixUtilisateur;
import fr.afpa.interfaces.controles.IControleCreationUtilisateur;
import fr.afpa.interfaces.controles.IControleGeneral;
import fr.afpa.interfaces.dto.IDTOUtilisateurs;
import fr.afpa.interfaces.services.IServiceCreation;
import fr.afpa.interfaces.services.IServiceGeneral;
import fr.afpa.interfaces.services.IServiceModification;
import fr.afpa.interfaces.services.IServiceModificationSalle;
import fr.afpa.interfaces.services.IServiceVisualisation;
import fr.afpa.interfaces.services.IServicesCreationSalle;

/**
 * Handles requests for the application home page.
 */
@CrossOrigin(value = "http://localhost:3000")
@RestController
public class HomeControllerRest {

	@Autowired
	private IServiceGeneral serviceGeneral;
	@Autowired
	private IServiceVisualisation serviceVisualisation;
	@Autowired
	private IServiceModification serviceModification;
	@Autowired
	private IServiceModificationSalle serviceModificationSalle;
	@Autowired
	private IServiceCreation serviceCreation;

	@Autowired
	private IDTOUtilisateurs dtoUtilisateur;

	@Autowired
	private IControleAuthentificationUtilisateur controleAuthentificationUtilisateur;
	@Autowired
	private IControleChoixUtilisateur controleChoixUtilisateur;
	@Autowired
	private IControleCreationUtilisateur controleCreationUtilisateur;
	@Autowired
	private IControleGeneral controleGeneral;
	@Autowired
	private IServicesCreationSalle serviceCreationSalle;

	/**
	 * Controller permettant de logguer la personne en fonction de son type de
	 * profil, admin ou utilisateur
	 * 
	 * @param login    de la personne
	 * @param password de la personne
	 * @return un model contenant l'utilisateur ou l'admin et la redirection vers
	 *         les menus associ�s
	 */
	/*@PostMapping(value = "/SAP")
	@ResponseBody
	public ModelAndView authentificationPersonneRest(@RequestParam(value = "login") String login,
			@RequestParam(value = "password") String password, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();

		if (controleAuthentificationUtilisateur.controlePersonneInscrite(login, password)) {
			Personne personne = serviceUtilisateur.utilisateur(login, password);
			if (personne instanceof Utilisateur) {
				mv.addObject("personne", personne);
				mv.setViewName("menuUser");
			} else {
				mv.addObject("personne", personne);
				mv.setViewName("gestionuser");
			}

		} else {
			mv.setViewName("index");

		}
		return mv;
	}*/

	/**
	 * Controller permettant d'afficher l'utilisateur dans la gestion des
	 * utilisateurs
	 * 
	 * @param choix
	 * @return
	 */
	@GetMapping(value = "/SChURest")
	@ResponseBody
	public Personne choixUserRest(@RequestParam(value="idUser") String choix) {
		Map<Integer, Personne> listePersonnes = dtoUtilisateur.listePersonnes();
		Optional<Personne> personne = Optional.ofNullable(listePersonnes.get(Integer.parseInt(choix)));
		if (personne.isPresent() && personne.get() instanceof Utilisateur) {
			return personne.get();
		}
		return null;
	}

	/**
	 * Controller permettant de cr�er une personne
	 * 
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param adresse
	 * @param role
	 * @param datenaissance
	 * @param password
	 * @param password2
	 * @param login
	 * @param create
	 * @return
	 */
	@PostMapping(value = "/SCURest")
	@ResponseBody
	public Personne createUserRest(@RequestBody CreateUser createUser) {
		String nomOk = "";
		String prenomOk = "";
		String mailOk;
		String adresseOk;
		RolePersonne roleOk = RolePersonne.STAGIAIRE;
		LocalDate dateNaissance = LocalDate.now();
		String loginOk = "";
		if (controleGeneral.controleNomPrenom(createUser.getNom())) {
			nomOk = createUser.getNom();
		}
		if (controleGeneral.controleNomPrenom(createUser.getPrenom())) {
			prenomOk = createUser.getPrenom();
		}
		mailOk = createUser.getMail();
		adresseOk = createUser.getAdresse();
		if (controleGeneral.controleRole(createUser.getRole())) {
			roleOk = serviceCreation.conversionRole(createUser.getRole());
		}
		if (controleGeneral.controleDateDeNaissance(createUser.getDateNaissance())) {
			dateNaissance = serviceGeneral.conversionDate(createUser.getDateNaissance());
		}
		if (createUser.getPassword().equals(createUser.getPassword2()) && controleCreationUtilisateur.controleLogin(createUser.getLogin())) {
			loginOk = createUser.getLogin();
		}
		if ("".equals(nomOk) || "".equals(prenomOk) || "".equals(mailOk) || "".equals(adresseOk)
				|| "".equals(loginOk)) {
			return null;
		} else {
			if ("user".equals(createUser.getCreate())) {
				return serviceCreation.creationPersonne(createUser.getNom(), createUser.getPrenom(), dateNaissance, createUser.getMail(), 
						createUser.getAdresse(), true, roleOk, createUser.getLogin(), createUser.getPassword(), false);
			} else if ("admin".equals(createUser.getCreate())) {
				return serviceCreation.creationPersonne(createUser.getNom(), createUser.getPrenom(), dateNaissance, createUser.getMail(), 
						createUser.getAdresse(), true, roleOk, createUser.getLogin(), createUser.getPassword(), true);
			} else if ("pageUser".equals(createUser.getCreate())) {
				return serviceCreation.creationPersonne(createUser.getNom(), createUser.getPrenom(), dateNaissance, createUser.getMail(), 
						createUser.getAdresse(), false, roleOk, createUser.getLogin(), createUser.getPassword(), false);
			}
		}
		return null;
	}


	/**
	 * Controller permettant de modifier un utilisateur (gestion de compte)
	 * 
	 * @param modif
	 * @param password
	 * @param password2
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param adresse
	 * @param datenaissance
	 * @param id
	 * @return
	 */
	@PutMapping(value = "/SMURest")
	public boolean modificationUtilisateurRest(@RequestBody ModifUser modifUser) {
		if (modifUser.getPassword().equals(modifUser.getPassword2())) {
			Personne user = new Utilisateur();
			user.setNom(modifUser.getNom());
			user.setPrenom(modifUser.getPrenom());
			user.setEmail(modifUser.getMail());
			user.setAdresse(modifUser.getAdresse());
			user.setRole(serviceCreation.conversionRole(modifUser.getRole()));
			user.setDateNaissance(serviceGeneral.conversionDate(modifUser.getDatenaissance()));
			return serviceModification.modifierUtilisateur(user, Integer.parseInt(modifUser.getId()), modifUser.getPassword());
		}
		return false;
	}
	
	
	@PutMapping(value = "/SMUActifRest")
	public boolean activationDesactivationUtilisateurRest(@RequestParam(value="idUser") int id) {
		return serviceModification.activerDesactiverUtilisateur(id);
	}
	
	@DeleteMapping(value = "/SMURest")
	public boolean suppressionUtilisateurRest(@RequestParam(value="idUser") int id) {
		return serviceModification.supprimerUtilisateur(id);
	}


	/**
	 * Controlleur permettant de visualiser la liste des utilisateur ( gestion
	 * utilisateur )
	 * 
	 * @return la liste des personnes
	 */
	@GetMapping(value = "/SVURest")
	@ResponseBody
	public List<Personne> visualisationUtilisateurRest() {
		Map<Integer, Personne> temp = serviceVisualisation.listeTousPersonnes();
		List<Personne> listePersonnes = new ArrayList<Personne>();
		for (Entry<Integer, Personne> personne : temp.entrySet()) {
			listePersonnes.add(personne.getValue());
		}
		return listePersonnes;
	}


	/**
	 * Controller permettant d'envoyer un message
	 * 
	 * @param destinataire
	 * @param objet
	 * @param contenu
	 * @return un boolean pour savoir si le destinaire et valide ou pas
	 */
	@PostMapping(value = "/EMRest")
	@ResponseBody
	public boolean envoyerMessageRest(@RequestBody EnvoyerMessage envoyerMessage) {
		if (controleAuthentificationUtilisateur.controleDestinataire(envoyerMessage.getDestinataire())) {
			return serviceCreation.creationMessage(envoyerMessage.getExpediteur(), envoyerMessage.getDestinataire(), envoyerMessage.getObjet(), envoyerMessage.getContenu(), LocalDateTime.now());
		} else {
			return false;
		}
	}

	/**
	 * Controller permettant d'achiver un message
	 * 
	 * @param id r�cup�ration de l'id pour mettre � true l'archivage du message dans
	 *           la base de donn�e
	 * @return redirection vers boite de r�ception
	 */
	@PutMapping(value = "/ARCRest")
	@ResponseBody
	public boolean archivageRest(@RequestBody Integer id) {
		return serviceModification.archiverMsg(id);
	}

	/**
	 * Controller permettant d'afficher la liste des messages archiver dans la page
	 * Messages archiv�s
	 * 
	 * @return le model contenant la liste des messages archiv�s et la redirection
	 */
	@GetMapping(value = "/MARest")
	@ResponseBody
	public List<Message> boiteArchiveRest(@RequestBody String expediteur) {
		List<Message> lm = serviceVisualisation.afficherListeMessage(expediteur);
		lm.addAll(serviceVisualisation.afficherListeMessageEnvoyer(expediteur));
		return lm;
	}

	/**
	 * Controller permettant d'afficher la liste des messages dans la boite de
	 * r�ception
	 * 
	 * @return les donn�es du model et la vue
	 */
	@GetMapping(value = "/BRRest")
	@ResponseBody
	public List<Message> boiteReceptionRest(@RequestBody String expediteur) {
		return serviceVisualisation.afficherListeMessage(expediteur);
	}

	/**
	 * Controller qui permet d'afficher la liste des messages envoy�s
	 * 
	 * @return les donn�es du model et la redirection
	 */
	@GetMapping(value = "/MERest")
	@ResponseBody
	public List<Message> messageEnvoyeRest(@RequestBody String expediteur) {
		return serviceVisualisation.afficherListeMessageEnvoyer(expediteur);
	}

	/**
	 * Controller permettant de visualiser un message envoy�
	 * 
	 * @param destinataire
	 * @param objet
	 * @param contenu
	 * @param date
	 * @return un model contenant le message et la redirection
	 */
	@GetMapping(value = "/voirERest")
	@ResponseBody
	public VoirMessage voirMessageRest(@RequestBody VoirMessage voirMessage) {
		return voirMessage;
	}

	@GetMapping(value = "/SChSRest")
	@ResponseBody
	public Salle choixSalleRest(@RequestParam(value="idSalle") String choix) {
		return serviceModificationSalle.getSalle(choix);
	}
	
	@PutMapping(value = "/MSRest")
	public boolean modificationSalleRest(@RequestBody ModifSalle modifSalle) {
		Salle salle = new Salle();
		salle.setNom(modifSalle.getNomsalle());
		salle.setCapacite(Integer.parseInt(modifSalle.getCapacite()));
		salle.setSurface(Float.parseFloat(modifSalle.getSurface()));
		salle.setNumero(modifSalle.getNumsalle());
//		salle.setTypeSalle();
		return serviceModificationSalle.updateSalle(salle);
	}

	/**
	 * Return la vue choix de la salle
	 */
	@GetMapping(value = "/csRest")
	@ResponseBody
	public List<Salle> choixSalleRest() {
		return serviceModificationSalle.voirSalle();
	}

	@GetMapping(value = "/vsRest")
	@ResponseBody
	public String voirSalleRest() {
		return serviceModificationSalle.listeSalleComplete();
	}

	@GetMapping(value = "/vrcRest")
	@ResponseBody
	public Salle voirSalleCompleteRest(@RequestBody String id) {
		return serviceModificationSalle.getSalleComplete(id);
	}

	
	@GetMapping(value = "/scRest")
	@ResponseBody
	public Map<String, Object> salleChoisiRest(@RequestBody ChoixSalle choixSalle) {
		Map<String, Object> mv = new HashMap<String, Object>();
		mv.put("id", choixSalle.getId());
		Salle salle = serviceModificationSalle.getSalle(choixSalle.getId());
		if (salle != null) {
			if ("Reserver".equals(choixSalle.getRes())) {
				mv.put("reservations", serviceVisualisation.listeReservations(Integer.parseInt(choixSalle.getId())));
			} else {
				mv.put("materiel", serviceModificationSalle.voirMateriel(Integer.parseInt(choixSalle.getId())));
				mv.put("salle", salle);
			}
		} else {
			mv.put("allroom", serviceModificationSalle.voirSalle());
		}
		return mv;
	}

	@GetMapping(value = "/crsRest")
	@ResponseBody
	public Map<String, Object> createSalleRest() {
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("listebatiment", serviceModificationSalle.listerBatiment());
		res.put("listeTypeSalle", TypeSalle.values());
		return res;
	}

	@PutMapping(value = "/rmsRest")
	@ResponseBody
	public boolean redirectionModifSalleRest(@RequestBody ModifSalle modifSalle) {
		switch (modifSalle.getModif()) {
		case "valider":
			Salle salle = new Salle(modifSalle.getNumsalle(), modifSalle.getNomsalle(), Integer.parseInt(modifSalle.getCapacite()), Float.parseFloat(modifSalle.getSurface()),
					TypeSalle.valueOf(modifSalle.getType().toUpperCase()));
			salle.setId(Integer.parseInt(modifSalle.getId()));
			return serviceModificationSalle.updateSalle(salle);
		case "supprimer":
			return serviceModificationSalle.supprimerSalle(Integer.parseInt(modifSalle.getId()));
		default:
			break;
		}
		return false;
	}

	/**
	 * @param batiment
	 * @param numero   de salle
	 * @param nom      de salle
	 * @param surface  de la salle
	 * @param capacite de la salle
	 * @param type     de salle Return la vue modification de la salle
	 */
	@PostMapping(value = "/asbddRest")
	@ResponseBody
	public boolean ajoutSalleBddRest(@RequestBody AjoutSalle ajoutSalle) {
		Salle salle = new Salle(ajoutSalle.getNumsalle(), ajoutSalle.getNomsalle(), Integer.parseInt(ajoutSalle.getCapacite()), Float.parseFloat(ajoutSalle.getSurface()),
				TypeSalle.values()[Integer.parseInt(ajoutSalle.getType())]);
		return serviceCreationSalle.ajoutSalleBdd(salle, ajoutSalle.getBatiment(), ajoutSalle.getType());
	}

	@PostMapping(value = "/ReserverRest")
	@ResponseBody
	public boolean reserverRest(@RequestBody ReserverInfo reserverInfo) {
		if (controleGeneral.controleDateDeNaissance(reserverInfo.getDebut()) && controleGeneral.controleTailleObjetMesage(reserverInfo.getIntitule())
				&& controleChoixUtilisateur.verificationChoix(reserverInfo.getDuree())
				&& controleGeneral.controleDatePasObsolete(serviceGeneral.conversionDate(reserverInfo.getDebut()))
				&& controleGeneral.controleCollisionDates(
						serviceVisualisation.listeReservations(Integer.parseInt(reserverInfo.getIdSalle())),
						serviceGeneral.conversionDate(reserverInfo.getDebut()), Integer.parseInt(reserverInfo.getDuree()))) {
			return serviceCreationSalle.creationReservation(reserverInfo.getIntitule(), serviceGeneral.conversionDate(reserverInfo.getDebut()),
					Integer.parseInt(reserverInfo.getDuree()), Integer.parseInt(reserverInfo.getIdSalle()));
		}
		return false;
	}

	
	/**
	 * Permet d'annuler une reservation si celle-ci n'est pas en cours ou
	 * n'est pas encore passee
	 * @param idReservation : id de la reservation a annuler
	 * @return true si la reservation a bien ete annulee et false sinon
	 */
	@DeleteMapping(value = "/AnnulerRest")
	@ResponseBody
	public boolean supprimerReservation(@RequestParam(value = "idReservation") int idReservation) {
		Reservation reserv = serviceVisualisation.chercheReservation(idReservation);
		if (reserv!=null && controleGeneral.controleReservationEnCours(reserv)) {
			return serviceModificationSalle.annulerReservation(idReservation);
		}
		else {
			return false;
		}
	}
	
	/**
	 * Permet de modifier une resevation
	 * @param modifReserv
	 * @return
	 */
	@PutMapping(value = "/ModifReservRest")
	@ResponseBody
	public boolean modifierReservation(@RequestBody ModifReserv modifReserv) {
		Reservation reserv = serviceVisualisation.chercheReservation(modifReserv.getIdReservation());
		Salle salle = serviceModificationSalle.getSalle(modifReserv.getIdSalle());
		List<Reservation> listeReservations = serviceVisualisation.listeReservations(Integer.parseInt(modifReserv.getIdSalle()));
		if (reserv!=null) {
			listeReservations.remove(reserv);
		}
		if (reserv!=null && salle!=null 
				&& controleGeneral.controleReservationEnCours(reserv)
				&& controleGeneral.controleDateDeNaissance(modifReserv.getDateDebut()) && controleGeneral.controleTailleObjetMesage(modifReserv.getIntitule())
				&& controleChoixUtilisateur.verificationChoix(modifReserv.getDuree())
				&& controleGeneral.controleDatePasObsolete(serviceGeneral.conversionDate(modifReserv.getDateDebut()))
				&& controleGeneral.controleCollisionDates(listeReservations,
						serviceGeneral.conversionDate(modifReserv.getDateDebut()), Integer.parseInt(modifReserv.getDuree()))) {
			Reservation reservation = new Reservation(modifReserv.getIntitule(), 
												serviceGeneral.conversionDate(modifReserv.getDateDebut()), 
												serviceGeneral.conversionDate(modifReserv.getDateDebut()).plusDays(Integer.parseInt(modifReserv.getDuree())));
			reservation.setId(modifReserv.getIdReservation());
			return serviceModificationSalle.modifierReservation(reservation, Integer.parseInt(modifReserv.getIdSalle()));
		}
		else {
			return false;
		}
	}

}
