package fr.afpa.entites;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class AjoutSalle {
	
	private String batiment;
	private String numsalle;
	private String nomsalle;
	private String surface;
	private String capacite;
	private String type;
	
}
