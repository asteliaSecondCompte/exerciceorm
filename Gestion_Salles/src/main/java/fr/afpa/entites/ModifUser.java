package fr.afpa.entites;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ModifUser {

	//private String modif;
	private String password;
	private String password2;
	private String nom;
	private String prenom;
	private String mail;
	private String adresse;
	private String role;
	private String datenaissance;
	private String id;
	
}
