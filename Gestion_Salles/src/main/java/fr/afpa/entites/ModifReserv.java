package fr.afpa.entites;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ModifReserv {

	private int idReservation;
	private String intitule;
	private String dateDebut;
	private String duree;
	private String idSalle;
	
}
