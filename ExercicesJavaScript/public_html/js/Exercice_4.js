
function choisir() {
    var semaine = document.getElementById("semaine");
    var weekEnd = document.getElementById("week-end");
    
    if (semaine.checked) {
        document.getElementById("valeur").value = semaine.value;
    }
    else {
        document.getElementById("valeur").value = weekEnd.value;
    }
}

function choisirBis() {
    var tab = document.getElementsByName("jours");
    for (i in tab) {
        if (tab[i].checked) {
            document.getElementById("valeur").value = tab[i].value;
        }
    }
}