
// Méthode 1
function controler() {
    if (document.getElementById("champ").value.length < 2) {
        alert("La chaine doit comporter au moins 2 caractères.");
    } else {
        alert("Vous avez saisi : '" + document.getElementById("champ").value + "'");
    }
}


// Méthode 2
document.getElementById("propriete").onclick = function () {
    if (document.getElementById("champ").value.length <= 1) {
        alert("La chaine doit comporter au moins 2 caractères.");
    } else {
        alert("Vous avez saisi : '" + document.getElementById("champ").value + "'");
    }
};


// Méthode 3
(function auto3() {
    document.getElementById("methode").addEventListener("click", function () {
        controler();
    });
}());


// Variante Mail
(function testEmail() {
    document.getElementById("testMail").addEventListener("click", function () {
        var mail = document.getElementById("mail").value;
        if (RegExp("^[a-z]+([\\.][a-z]+)*@[a-z]+\\.[a-z]{2,3}$").test(mail)) {
            alert("Vous avez saisi : '" + document.getElementById("mail").value + "'");
        }
        else {
            alert("Le mail est invalide.");
        }
    });
}());
