(function auto() {
    document.getElementById("cache").addEventListener("click", function () {
    document.getElementById("postIt").setAttribute("class", "invisible");
});
    document.getElementById("affiche").addEventListener("click", function () {
    document.getElementById("postIt").innerHTML = "Vous avez cliqué sur le bouton 'Affiche' !";
    document.getElementById("postIt").setAttribute("class", "visible");
});
    document.getElementById("survol").addEventListener("mouseover", function () {
    document.getElementById("postIt").innerHTML = "C'est gentil de me survoler...";
    document.getElementById("postIt").setAttribute("class", "visible");
});
    document.getElementById("survol").addEventListener("mouseout", function () {
    document.getElementById("postIt").setAttribute("class", "invisible");
});
}());

/*
function cacher() {
    document.getElementById("postIt").setAttribute("class", "invisible");
}

function afficher() {
    document.getElementById("postIt").innerHTML = "Vous avez cliqué sur le bouton 'Affiche' !";
    document.getElementById("postIt").setAttribute("class", "visible");
}

function survoler() {
    document.getElementById("postIt").innerHTML = "C'est gentil de me survoler...";
}*/