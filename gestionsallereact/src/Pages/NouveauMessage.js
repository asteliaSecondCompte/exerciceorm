import React from 'react';
import { Container, Form } from 'react-bootstrap';
import MenuMessagerie from '../Composants/MenuMessagerie';

export default function NouveauMessage() {

    return (
        <Container>
            <MenuMessagerie />
            <br />
            <h2 className="texteCentre">Nouveau Message</h2>
            <br />
            <Container id="messageErreur" className="btn-danger">
                Le message n'a pas pu être envoyé.
            </Container>
            <br />
            <Form action="/boiteReception">
                <Container>
                    <Form.Control id="destinataire" type="text" placeholder="A :" /><br />
                    <Form.Control id="objet" type="text" placeholder="Objet :" /><br />
                    <Form.Control id="message" as="textarea" placeholder="Votre message :" /><br />
                </Container>
                <input className="btn btn-primary" type="submit" value="Envoyer" />
            </Form>
        </Container>
    );

}