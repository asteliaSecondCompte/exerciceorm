import React from 'react';
import { Container } from 'react-bootstrap';
import MenuMessagerie from '../Composants/MenuMessagerie';
import RecupMessages from '../Composants/RecupMessages';

export default function MessagesEnvoyes() {

    return (
        <Container>
            <MenuMessagerie/>
            <br/>
            <h2 className="texteCentre">Messages Envoyés</h2>
            <br/>
            <RecupMessages value="envoye" />
        </Container>
    );

}