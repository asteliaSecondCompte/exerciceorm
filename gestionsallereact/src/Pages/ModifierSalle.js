import React, { useState, useEffect, Fragment } from 'react';
import { Container, Form, Col } from 'react-bootstrap';
import MenuSalles from '../Composants/MenuSalles';
import { Link, Redirect } from 'react-router-dom';

export default function CreationSalle() {
    const [salle, setSalle] = useState({});
    const [pret, setPret] = useState(false);
    const [nom, setNom] = useState("");
    const [type, setType] = useState("");
    const [batiment, setBatiment] = useState("");
    const [numero, setNumero] = useState("");
    const [surface, setSurface] = useState(0);
    const [capacite, setCapacite] = useState(0);
    const [status, setStatus] = useState(false);

    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handleNumero = (evt) => {
        setNumero(evt.target.value);
    }

    const handleSurface = (evt) => {
        setSurface(evt.target.value);
    }

    const handleCapacite = (evt) => {
        setCapacite(evt.target.value);
    }
    
    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await getSalle(setSalle, setNom, setNumero, setSurface, setType, setCapacite, setBatiment);
            setPret(true);
        }
        fetchJson();
        getSalle(setSalle, setNom, setNumero, setSurface, setType, setCapacite, setBatiment);
    }, []);
    return <Fragment>
        {
            status ? (<Redirect to="listePersonnes" />) :
                pret ?
                    (
                        <Container>
                            <MenuSalles />
                            <br /><br />
                            <h2>Modification de la salle<Form.Label id="idSalle"></Form.Label></h2>
                            <br />
                            <Container>
                                <Form action="listeSalles">
                                    <Form.Group>
                                        <Form.Label htmlFor="batiment">Nom de Bâtiment</Form.Label><br />
                                        <Form.Control id="batiment" as="select">
                                            <option id="a">A</option>
                                            <option id="b">B</option>
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label htmlFor="type">Type de salle</Form.Label><br />
                                        <Form.Control id="type" as="select">
                                            <option id="reunion">Réunion</option>
                                            <option id="informatique">Informatique</option>
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label htmlFor="nom">Nom de salle</Form.Label>
                                        <Form.Control id="nom" type="text" placeholder="ex : Watch Tower" value={salle.nom} onChange={handleNom} />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label htmlFor="numero">Numéro de salle</Form.Label>
                                        <Form.Control id="numero" type="text" step="1" placeholder="ex : 704" value={salle.numero} onChange={handleNumero} />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label htmlFor="surface">Surface (en m²)</Form.Label>
                                        <Form.Control id="surface" type="number" step="0.001" placeholder="ex : 100" value={salle.surface} onChange={handleSurface} />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label htmlFor="capacite">Capacite</Form.Label>
                                        <Form.Control id="capacite" type="number" step="1" placeholder="ex : 25" value={salle.capacite} onChange={handleCapacite} />
                                    </Form.Group>
                                    <br />
                                    {materiel()}
                                    <br />
                                    <Link to="/listeSalles"><input className="btn btn-success" type="button" value="Valider" onClick={() => modifierSalle(nom, type, batiment, numero, surface, capacite, setStatus, "valider")} /></Link>
                                    <Link to="/listeSalles"><input className="btn btn-danger" type="button" value="Supprimer" /></Link>
                                    <br />
                                </Form>
                            </Container>
                        </Container>
                    ) : (<div></div>)}
    </Fragment>
}

function materiel() {
    return (
        <Col md="auto" className="tableauxSalle col-md-6 col-sm-12">
            <table id="materiel">
                <thead>
                    <tr>
                        <th>Matériel</th>
                        <th>Quantité</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Chaise(s)</td>
                        <td><input id="nbChaises" type="number" step="1" placeholder="ex : 25" /></td>
                    </tr>
                </tbody>
            </table>
        </Col>
    );
}

function modifierSalle(nom, type, batiment, numero, surface, capacite, setStatus, modif) {
    let idSalle = parseInt(document.URL.split("=")[1]);
    fetch(`${process.env.REACT_APP_API_URL}/MSRest`, {
        method: "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json'
        },
        body: `{
                "id": "${idSalle}",
                "nom": "${ nom}",
                "type": "${ type}",
                "batiment": "${ batiment}",
                "numero": "${ numero}",
                "surface": "${ surface}",
                "capacite": "${ capacite}",
                "modif": "${ modif}"
            }`
    })
        .then(response => {
            let temp = response.json();
            setStatus(true);
            return temp
        })
        .catch(error => {
            console.log(error);
            setStatus(false);
        })

}

async function getSalle(setSalle) {
    let idSalle = parseInt(document.URL.split("=")[1]);
    await fetch(`${process.env.REACT_APP_API_URL}/SChSRest?idSalle=${idSalle}`)
        .then(response => response.json())
        .then(json => {
            setSalle(json);
            console.log(json);
        })
        .catch(error => console.log(error))
}
