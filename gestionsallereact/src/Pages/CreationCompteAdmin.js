import React, { useState, Fragment } from 'react';
import MenuPersonnes from '../Composants/MenuPersonnes';
import { Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

export default function CreationCompteAdmin() {

    const [nom, setNom] = useState();
    const [prenom, setPrenom] = useState();
    const [email, setEmail] = useState();
    const [adresse, setAdresse] = useState();
    const [role, setRole] = useState();
    const [dateN, setDateN] = useState();
    const [login, setLogin] = useState();
    const [mdp, setMDP] = useState();
    const [mdpVerif, setMDPVerif] = useState();
    const [status, setStatus] = useState(false);

    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handlePrenom = (evt) => {
        setPrenom(evt.target.value);
    }

    const handleEmail = (evt) => {
        setEmail(evt.target.value);
    }

    const handleAdresse = (evt) => {
        setAdresse(evt.target.value);
    }

    const handleRole = (evt) => {
        setRole(evt.target.value);
    }

    const handleDateN = (evt) => {
        setDateN(evt.target.value);
    }

    const handleLogin = (evt) => {
        setLogin(evt.target.value);
    }

    const handleMDP = (evt) => {
        setMDP(evt.target.value);
    }

    const handleMDPVerif = (evt) => {
        setMDPVerif(evt.target.value);
    }

    return <Fragment>
        {status ? (<Redirect to="listePersonnes" />) :
            (<div className="container">
                <br />
                <MenuPersonnes />
                <br />
                <h2>Création d'un compte</h2>
                <br />
                <div className="container">
                    <form id="compte" className="form-group">
                        <div>
                            <label htmlFor="nom">Nom</label>
                            <input id="nom" className="form-control" type="text" placeholder="ex : Wayne" onChange={handleNom} />
                        </div>
                        <br />
                        <div>
                            <label htmlFor="prenom">Prénom</label>
                            <input id="prenom" className="form-control" type="text" placeholder="ex : Bruce" onChange={handlePrenom} />
                        </div>
                        <br />
                        <div>
                            <label htmlFor="email">Email</label>
                            <input id="email" className="form-control" type="mail" placeholder="ex : batman@gmal.com" onChange={handleEmail} />
                        </div>
                        <br />
                        <div>
                            <label htmlFor="adresse">Adresse</label>
                            <input id="adresse" className="form-control" type="text" placeholder="ex : Manoir Wayne, Gotham City" onChange={handleAdresse} />
                        </div>
                        <br />
                        <div>
                            <label htmlFor="role">Rôle</label><br />
                            <select id="role" className="form-control" onChange={handleRole}>
                                <option id="stagiaire">Stagiaire</option>
                                <option id="formateur">Formateur</option>
                            </select>
                        </div>
                        <br />
                        <div>
                            <label htmlFor="dateN">Date de naissance</label>
                            <input id="dateN" className="form-control" type="date" placeholder="ex : 01/04/1998" onChange={handleDateN} />
                        </div>
                        <br />
                        <div>
                            <label htmlFor="login">Login</label>
                            <input id="login" className="form-control" type="text" placeholder="ex : batman" onChange={handleLogin} />
                        </div>
                        <br />
                        <div>
                            <label htmlFor="mdp">Mot de passe</label>
                            <input id="mdp" className="form-control" type="password" placeholder="ex : batarang" onChange={handleMDP} />
                        </div>
                        <br />
                        <div>
                            <label htmlFor="mdpVerif">Valider mot de passe</label>
                            <input id="mdpVerif" className="form-control" type="password" placeholder="ex : batarang" onChange={handleMDPVerif} />
                        </div>
                        <br />
                        <Button className="btn btn-primary" onClick={() => creerUtilisateur(nom, prenom, email, adresse, role, dateN, login, mdp, mdpVerif, setStatus)}>Créer utilisateur</Button>
                        <Button className="btn btn-primary" onClick={() => creerAdministrateur(nom, prenom, email, adresse, role, dateN, login, mdp, mdpVerif, setStatus)}>Créer administrateur</Button>
                    </form>
                </div>
            </div>
            ) }
    </Fragment>
}


function creerUtilisateur(nom, prenom, email, adresse, role, dateN, login, mdp, mdpVerif, setStatus) {
    fetch(`${process.env.REACT_APP_API_URL}/SCURest`, {
        method: "post",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json'
        },
        body: `{
                "nom": "${ nom}",
                "prenom": "${ prenom}",
                "mail": "${ email}",
                "adresse": "${ adresse}",
                "role": "${ role}",
                "dateNaissance": "${ dateN }",
                "login": "${ login}",
                "password": "${ mdp}",
                "password2": "${ mdpVerif}",
                "create": "user"
            }`
    })
        .then(response => {
            let temp = response.json();
            setStatus(true);
            return temp
        })
        .catch(error => {
            console.log(error);
            setStatus(false);
        })

}

function creerAdministrateur(nom, prenom, email, adresse, role, dateN, login, mdp, mdpVerif, setStatus) {
    fetch(`${process.env.REACT_APP_API_URL}/SCURest`, {
        method: "post",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json'
        },
        body: `{
                "nom": "${ nom}",
                "prenom": "${ prenom}",
                "mail": "${ email}",
                "adresse": "${ adresse}",
                "role": "${ role}",
                "datenaissance": "${ dateN}",
                "login": "${ login}",
                "password": "${ mdp}",
                "password2": "${ mdpVerif}",
                "create": "admin"
            }`
    })
        .then(response => { 
            let temp = response.json();
            setStatus(true);
            return temp
         })
        .catch(error => { 
            console.log(error);
            setStatus(false);
         })
}