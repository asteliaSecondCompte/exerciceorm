import React from 'react';
import { Link } from 'react-router-dom';

export default function MenuAdmin() {
    return (
        <div className="container texteCentre">
            <br />
            <h2>Menu Administrateur</h2>
            <br />
            <Link to="/listeSalles">Gérer les salles</Link><br /><br />
            <Link to="/boiteReception">Messagerie</Link><br /><br />
            <Link to="/listePersonnes">Gérer les utilisateurs</Link><br /><br />
            <Link to="/"><input className="btn btn-danger" type="button" value="Déconnexion" /></Link>
        </div>
    );
}