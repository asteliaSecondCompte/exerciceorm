import React from 'react';
import MenuSalles from '../Composants/MenuSalles';
import { Col, Row, Container } from 'react-bootstrap';

function SalleComplete(props) {
    return (
        <div>
            <MenuSalles />
            <br />
            <h2 className="texteCentre">Salle <label id="nomSalle">nom Salle</label></h2><br />
            {recupSalle(props.id)}
        </div>
    );
}

export default SalleComplete;


function bns() {
    return (
        <Col className="divFlottante gauche col-md-6 col-sd-12">
            <label>Bâtiment : {"A"}</label><br />
            <label>Nom : {"Watch Tower"}</label><br />
            <label>Surface : {100} m²</label><br />
        </Col>
    );
}

function tc() {
    return (
        <Col className="divFlottante droite col-md-6 col-sd-12">
            <label>Type : {"Réunion"}</label><br />
            <label>Capacité : {25} personnes</label><br />
        </Col>
    );
}

function materiel() {
    return (
        <Col md="auto" className="gauche tableauxSalle col-md-6 col-sm-12">
            <table id="materiely">
                <thead>
                    <tr>
                        <th>Matériel</th>
                        <th>Quantité</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Chaise(s)</td>
                        <td>25</td>
                    </tr>
                </tbody>
            </table>
        </Col>
    );
}

function reservations() {
    return (
        <Col md="auto" className="droite tableauxSalle col-md-6 col-sm-12">
            <table id="reservation">
                <thead>
                    <tr>
                        <th>Intitulé</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Gotham Sirens</td>
                        <td>01/04/2020</td>
                        <td>05/04/2020</td>
                    </tr>
                </tbody>
            </table>
        </Col>
    )
}

function recupSalle(id) {
    return (
        <Container>
            <Row className="justify-content-md-center">
                {bns()}
                {tc()}
            </Row>
            <br/>
            <Row className="justify-content-md-center">
                {materiel()}
                {reservations()}
            </Row>
        </Container>
    )
}