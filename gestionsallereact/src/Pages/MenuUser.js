import React from 'react';
import { Link } from 'react-router-dom';
import RecupSalles from '../Composants/RecupSalles';

export default function MenuUser() {
    return (
        <div className="container texteCentre">
            <br />
            <h2>Bonjour</h2>
            <br />
            <RecupSalles/>
            <br/>
            <footer>
                <Link to=""><input className="btn btn-danger" type="button" value="Déconnexion"/></Link>
            </footer>
            
        </div>
    );
}