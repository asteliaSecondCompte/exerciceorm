import React from 'react';

export default function Authentification() {

    return (
        <div className="container">
            <br/>
            <h2>Authentification</h2>
            <br /><br />
            <div className="container">
                <form action="/menuAdmin" className="form-group">
                    <div className="input-group">
                        <input id="login" className="form-control" type="text" placeholder="Login" />
                    </div>
                    <br /><br />
                    <div className="input-group">
                        <input id="mdp" className="form-control" type="text" placeholder="Mot de passe" />
                    </div>
                    <br /><br />
                    <input className="btn btn-primary" type="submit" value="Connexion" />
                    <br /><br />
                    <a href="http://localhost:3000/creationCompte">Création compte utilisateur</a>
                </form>
            </div>
        </div>
    );

}
