import React, { useState } from 'react';
import { Container, Form,Button} from 'react-bootstrap';
import MenuSalles from '../Composants/MenuSalles';

export default function CreationSalle() {
    const [batiment, setBatiment] = useState(1);
    const [type, setType] = useState(1);
    const [nom, setNom] = useState();
    const [numero, setNumero] = useState();
    const [surface , setSurface] = useState();
    const [capacite , setCapacite] = useState();

    const handleBatiment = (evt) => {
        setBatiment(evt.target.value);
    }

    const handleType = (evt) => {
        setType(evt.target.value);
    }

    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handleNumero = (evt) => {
        setNumero(evt.target.value);
    }

    const handleSurface = (evt) => {
        setSurface(evt.target.value);
    }

    const handleCapacite = (evt) => {
        setCapacite(evt.target.value);
    }

    return (
        <Container>
            <MenuSalles />
            <br /><br />
            <h2>Création d'une salle</h2>
            <br />
            <Container>
                <Form action="listeSalles">
                    <div className="row">
                        <div className="col-sm-6">
                        <Form.Group>
                            <Form.Label htmlFor="batiment">Nom de Bâtiment</Form.Label><br />
                            <Form.Control id="batiment" as="select" onChange={handleBatiment}>
                                <option id="a" value="1">A</option>
                                <option id="b" value="2">B</option>
                                </Form.Control>
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group>
                                <Form.Label htmlFor="type">Type de salle</Form.Label><br />
                                <Form.Control id="type" as="select" onChange={handleType}>
                                    <option id="BUREAU" value="1">bureau</option>
                                    <option id="FORMATION" value="2">formation</option>
                                    <option id="INFIRMERIE" value="3">infirmerie</option>
                                    <option id="REUNION" value="4">reunion</option>
                                </Form.Control>
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group>
                                <Form.Label htmlFor="nom">Nom de salle</Form.Label>
                                <Form.Control id="nom" type="text" placeholder="ex : Watch Tower" onChange={handleNom}/>                        
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group>
                                <Form.Label htmlFor="numero">Numéro de salle</Form.Label>
                                <Form.Control id="numero" type="number" min = "0" step="1" placeholder="ex : 704" onChange={handleNumero}/>
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group>
                                <Form.Label htmlFor="surface">Surface (en m²)</Form.Label> 
                                <Form.Control id="surface" type="number" min = "0" step="0.001" placeholder="ex : 100" onChange={handleSurface}/>
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group>
                                <Form.Label htmlFor="capacite">Capacite</Form.Label>
                                <Form.Control id="capacite" type="number" min = "0" step="1" placeholder="ex : 25" onChange={handleCapacite}/>
                            </Form.Group>
                        </div>
                    </div>
                    <br />
                    <Button className="btn btn-primary" onClick={() => creerSalle(batiment,type,nom,numero,surface,capacite)} >Créer</Button>
                    <br/>
                </Form>
            </Container>
        </Container>
    );
}
function creerSalle(batiment,type,nom,numero,surface,capacite) {
    fetch(`${process.env.REACT_APP_API_URL}/asbddRest`, {
        method: "post",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json'
        },
        body: `{
                "batiment": "${ batiment}",
                "type": "${ type}",
                "nomsalle": "${ nom}",
                "numsalle": "${ numero}",
                "surface": "${ surface}",
                "capacite": "${ capacite }"
            }`
    })
        .then(response => {
            let temp = response.json();
            
            return temp
        })
        .catch(error => {
            console.log(error);
        })

}