import React from 'react';
import MenuSalles from '../Composants/MenuSalles';
import RecupSalles from '../Composants/RecupSalles';

function ListeSalles() {
    return (
        <div className="container">
            <MenuSalles />
            <br />
            <h2 className="texteCentre">Liste des salles</h2><br/>
            <div>
                <RecupSalles/>
            </div>
        </div>
    );
}

export default ListeSalles;


