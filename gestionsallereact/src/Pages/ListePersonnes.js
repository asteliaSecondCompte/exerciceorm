import React from 'react';
import { Container } from 'react-bootstrap';
import RecupPersonnes from '../Composants/RecupPersonnes';
import MenuPersonnes from '../Composants/MenuPersonnes';

export default function ListePersonnes() {
    return (
        <Container className="texteCentre">
            <br />
            <MenuPersonnes/>
            <br />
            <div>
                <h2>Liste des personnes</h2>
            </div>
            <br/>
            <RecupPersonnes />
        </Container>
    );
}