import React from 'react';
import { Container, Col, Form, Row } from 'react-bootstrap';
import MenuSalles from '../Composants/MenuSalles';

export default function ReserverSalle() {
    return (
        <Container>
            <MenuSalles />
            <br /><br />
            <h2>Réserver la salle <label id="idSalle"></label></h2>
            <br />
            <div id="message"></div>
            <Container>
                <h6>Liste des réservations de la salle <label id="idSalle"></label></h6>
            </Container>
            {reservations()}
            <hr />
            <Form action="listeSalles">
                <Row>
                    <Col md="3" xs="9">
                        <Form.Label htmlFor="nom">Intitulé</Form.Label>
                        <Form.Control id="intitule" type="text" placeholder="ex : Watch Tower" />
                    </Col>
                    <Col md="3" xs="9">
                        <Form.Label htmlFor="numero">Date de début</Form.Label>
                        <Form.Control id="debut" type="date" />
                    </Col>
                    <Col md="3" xs="9">
                        <Form.Label htmlFor="surface">Date de fin</Form.Label>
                        <Form.Control id="fin" type="date" />
                    </Col>
                </Row>
                <br/>
                <a href="/listeSalles">
                    <input className="btn btn-success" type="button" value="Réserver" />
                </a>
            </Form>
        </Container>
    );
}

function reservations() {
    return (
        <Col className="tableauxSalle">
            <table id="reservation">
                <thead>
                    <tr>
                        <th>Intitulé</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Gotham Sirens</td>
                        <td>01/04/2020</td>
                        <td>05/04/2020</td>
                    </tr>
                </tbody>
            </table>
        </Col>
    )
}