import React, { Fragment } from 'react';
import MenuPersonnes from '../Composants/MenuPersonnes';
import RecupPersonnes from '../Composants/RecupPersonnes';
import { InputGroup, FormControl, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

class ChoixUser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {val: null, user: null};
    }

    inputValue = (evt) => {
        this.setState({val : evt.target.value})
    }

    handleModif = () => {
        this.recupUser(this.state.val, this.setState)
    }

    recupUser = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/SChURest?idUser=${ this.state.val }`)
            .then(response => response.json())
            .then(json => 
                    {
                        this.setState({user: json});
                        if (this.state.user != null) {
                            return (<Redirect to={"modifierUser?userId="+ this.state.val} />)
                        }
                    }
                )
            .catch( error => console.log(error))
    }

    render() {
        return <Fragment>
        { (this.state.user === null || this.state.user === {}) ?
        (
            <div className="container">
                <MenuPersonnes />
                <br />
                <h2 className="texteCentre">Choix d'un utilisateur</h2><br />
                <div>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text>Id utilisateur</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl id="idUser" type="number" placeholder="ex : 1" onChange={ this.inputValue } />
                        <Button className="btn btn-warning" onClick={ this.handleModif }>Modifier</Button>
                    </InputGroup>
                </div>
                <br />
                <div>
                    <RecupPersonnes />
                </div>
            </div>
        ) : (<Redirect to={"modifierUser?userId="+ this.state.val} />)}
        </Fragment>
    }
}

export default ChoixUser;

