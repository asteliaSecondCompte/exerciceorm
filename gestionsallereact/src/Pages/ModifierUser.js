import React, { useEffect, Fragment } from 'react';
import MenuPersonnes from '../Composants/MenuPersonnes';
import { useState } from 'react';
import { Redirect } from 'react-router-dom';

export default function CreationCompteAdmin() {

    const [user, setUser] = useState({});
    const [pret, setPret] = useState(false);
    const [nom, setNom] = useState("");
    const [prenom, setPrenom] = useState("");
    const [email, setEmail] = useState("");
    const [adresse, setAdresse] = useState("");
    const [role, setRole] = useState("");
    const [dateN, setDateN] = useState("");
    const [mdp, setMDP] = useState("");
    const [mdpVerif, setMDPVerif] = useState("");
    const [status, setStatus] = useState(false);

    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handlePrenom = (evt) => {
        setPrenom(evt.target.value);
    }

    const handleEmail = (evt) => {
        setEmail(evt.target.value);
    }

    const handleAdresse = (evt) => {
        setAdresse(evt.target.value);
    }

    const handleRole = (evt) => {
        setRole(evt.target.value);
    }

    const handleDateN = (evt) => {
        setDateN(evt.target.value);
    }

    const handleMDP = (evt) => {
        setMDP(evt.target.value);
    }

    const handleMDPVerif = (evt) => {
        setMDPVerif(evt.target.value);
    }

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await getUser(setUser, setNom, setPrenom, setEmail, setAdresse, setRole, setDateN);
            setPret(true);
        }
        fetchJson();
        getUser(setUser, setNom, setPrenom, setEmail, setAdresse, setRole, setDateN);
    }, []);

    return <Fragment>
        {
            status ? (<Redirect to="listePersonnes" />) :
                pret ?
                    (
                        <div className="container">
                            <br />
                            <MenuPersonnes />
                            <br />
                            <h2>Modification d'un utilisateur</h2>
                            <br />
                            <div className="container">
                                <form className="form-group">
                                    <div>
                                        <label htmlFor="nom">Nom</label>
                                        <input id="nom" className="form-control" type="text" value={user.nom} onChange={handleNom} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="prenom">Prénom</label>
                                        <input id="prenom" className="form-control" type="text" value={user.prenom} onChange={handlePrenom} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="email">Email</label>
                                        <input id="email" className="form-control" type="mail" value={user.email} onChange={handleEmail} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="adresse">Adresse</label>
                                        <input id="adresse" className="form-control" type="text" value={user.adresse} onChange={handleAdresse} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="role">Rôle</label><br />
                                        <select id="role" className="form-control" onChange={handleRole}>
                                            <option>{user.role}</option>
                                            <option id="stagiaire">Stagiaire</option>
                                            <option id="formateur">Formateur</option>
                                        </select>
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="dateN">Date de naissance</label>
                                        <input id="dateN" className="form-control" type="date" value={conversionDate(user.dateNaissance.dayOfMonth, user.dateNaissance.monthValue, user.dateNaissance.year)}
                                            onChange={handleDateN} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="mdp">Mot de passe</label>
                                        <input id="mdp" className="form-control" type="password" onChange={handleMDP} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="mdpVerif">Valider mot de passe</label>
                                        <input id="mdpVerif" className="form-control" type="password" onChange={handleMDPVerif} />
                                    </div>
                                    <br />
                                    <input className="btn btn-success" type="button" value="Valider" onClick={() => modifierUtilisateur(nom, prenom, email, adresse, role, dateN, mdp, mdpVerif, setStatus, "valider")} />
                                    <input className="btn btn-primary" type="button" value="Activer/Désactiver" onClick={() => activerDesactiver(setStatus)} />
                                    <input className="btn btn-danger" type="button" value="Supprimer" onClick={ () => supprimerUtilisateur(setStatus) } />
                                </form>
                            </div>
                        </div>
                    ) : (<div></div>)}
    </Fragment>
}

function conversionDate(jour, mois, annee) {
    let sJour = "" + jour;
    let sMois = "" + mois;
    let sAnnee = "" + annee;
    if (sJour.length < 2) {
        sJour = 0 + sJour;
    }
    if (sMois.length < 2) {
        sMois = 0 + sMois;
    }
    if (sAnnee.length < 4) {
        while (sAnnee.length) {
            sAnnee = 0 + sAnnee;
        }
    }
    return sAnnee + "-" + sMois + "-" + sJour;
}

function supprimerUtilisateur(setStatus) {
    let idUser = parseInt(document.URL.split("=")[1]);
    fetch(`${process.env.REACT_APP_API_URL}/SMURest?idUser=${idUser}`, {
        method: "delete"
    })
        .then(response => {
            return response.json();
        })
        .then(json => setStatus(json))
        .catch(error => {
            console.log(error);
            setStatus(false);
        })
}

function activerDesactiver(setStatus) {
    let idUser = parseInt(document.URL.split("=")[1]);
    fetch(`${process.env.REACT_APP_API_URL}/SMUActifRest?idUser=${idUser}`, {
        method: "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json'
        }
    })
        .then(response => {
            return response.json();
        })
        .then(json => setStatus(json))
        .catch(error => {
            console.log(error);
            setStatus(false);
        })
}

function modifierUtilisateur(nom, prenom, email, adresse, role, dateN, mdp, mdpVerif, setStatus, modif) {
    let idUser = parseInt(document.URL.split("=")[1]);
    fetch(`${process.env.REACT_APP_API_URL}/SMURest`, {
        method: "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json'
        },
        body: `{
                "id": ${idUser},
                "nom": "${ nom}",
                "prenom": "${ prenom}",
                "mail": "${ email}",
                "adresse": "${ adresse}",
                "datenaissance": "${ conversionDate(dateN.dayOfMonth, dateN.monthValue, dateN.year)}",
                "role": "${ role}",
                "password": "${ mdp}",
                "password2": "${ mdpVerif}"
            }`
    })
        .then(response => {
            return response.json();
        })
        .then(json => setStatus(json))
        .catch(error => {
            console.log(error);
            setStatus(false);
        })

}

async function getUser(setUser, setNom, setPrenom, setEmail, setAdresse, setRole, setDateN) {
    let idUser = parseInt(document.URL.split("=")[1]);
    await fetch(`${process.env.REACT_APP_API_URL}/SChURest?idUser=${idUser}`)
        .then(response => response.json())
        .then(json => {
            setUser(json);
            console.log(json);
            setNom(json.nom);
            setPrenom(json.prenom);
            setEmail(json.email);
            setAdresse(json.adresse);
            setRole(json.role);
            setDateN(json.dateNaissance);
        })
        .catch(error => console.log(error))
}
