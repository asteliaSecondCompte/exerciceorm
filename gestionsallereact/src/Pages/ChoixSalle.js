import React, { Fragment } from 'react';
import MenuSalles from '../Composants/MenuSalles';
import RecupSalles from '../Composants/RecupSalles';
import { InputGroup, FormControl } from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';

class ChoixSalle extends React.Component {

    constructor(props) {
        super(props);
        this.state = { val: null, salle: null };
    }

    inputValue = (evt) => {
        this.setState({ val: evt.target.value })
    }

    handleModif = () => {
        this.recupSalle(this.state.val, this.setState)
    }

    recupSalle = () => {
        fetch(`${process.env.REACT_APP_API_URL}/SChSRest?idSalle=${this.state.val}`)
            .then(response => response.json())
            .then(json => {
                this.setState({ salle: json });
                if (this.state.salle != null) {
                    return (<Redirect to={"modifierSalle?salleId=" + this.state.val} />)
                }
            }
            )
            .catch(error => console.log(error))
    }

    render() {
        return <Fragment>
            {(this.state.salle === null || this.state.salle === {}) ?
                (
                    <div className="container">
                        <MenuSalles />
                        <br />
                        <h2 className="texteCentre">Choix d'une salle</h2><br />
                        <div>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>Id salle</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl id="idSalle" type="number" placeholder="ex : 1" onChange={this.inputValue} />
                                <Link to={"modifierSalle?idSalle=" + this.state.val}>
                                    <input className="btn btn-warning" type="button" value="Modifier" />
                                </Link>
                                <Link to={"reserverSalle?idSalle=" + this.state.val} >
                                    <input className="btn btn-primary" type="button" value="Réserver" />
                                </Link>
                            </InputGroup>
                        </div>
                        <br />
                        <div>
                            <RecupSalles />
                        </div>
                    </div>
                ) : (<Redirect to={"modifierSalle?salleId=" + this.state.val} />)}


        </Fragment>
    }
}

export default ChoixSalle;