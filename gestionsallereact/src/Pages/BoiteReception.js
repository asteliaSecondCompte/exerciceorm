import React from 'react';
import { Container } from 'react-bootstrap';
import MenuMessagerie from '../Composants/MenuMessagerie';
import RecupMessages from '../Composants/RecupMessages';

export default function BoiteReception() {

    return (
        <Container>
            <MenuMessagerie/>
            <br/>
            <h2 className="texteCentre">Boîte de Réception</h2>
            <br/>
            <RecupMessages value="recu" />
        </Container>
    );

}