import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import Authentification from '../Pages/Authentification';
import CreationCompte from '../Pages/CreationCompte';
import MenuAdmin from '../Pages/MenuAdmin';
import ListeSalles from '../Pages/ListeSalles';
import SalleComplete from '../Pages/SalleComplete';
import CreationSalle from '../Pages/CreationSalle';
import ChoixSalle from '../Pages/ChoixSalle';
import ReserverSalle from '../Pages/ReserverSalle';
import ModifierSalle from '../Pages/ModifierSalle';
import ListePersonnes from '../Pages/ListePersonnes';
import CreationCompteAdmin from '../Pages/CreationCompteAdmin';
import ChoixUser from '../Pages/ChoixUser';
import ModifierUser from '../Pages/ModifierUser';
import BoiteReception from '../Pages/BoiteReception';
import MessagesEnvoyes from '../Pages/MessageEnvoyes';
import MessagesArchives from '../Pages/MessagesArchives';
import VoirMessage from '../Pages/VoirMessage';
import NouveauMessage from '../Pages/NouveauMessage';
import MenuUser from '../Pages/MenuUser';

export default function Chemin() {
    return (
        <Router>
            <div>
                {/* A <Switch> looks through its children <Route>s and
                renders the first one that matches the current URL. */}
                <Switch>
                    <Route path="/nouveauMessage">
                        <NouveauMessage />
                    </Route>
                    <Route path="/voirMessage">
                        <VoirMessage />
                    </Route>
                    <Route path="/messagesArchives">
                        <MessagesArchives />
                    </Route>
                    <Route path="/messagesEnvoyes">
                        <MessagesEnvoyes />
                    </Route>
                    <Route path="/boiteReception">
                        <BoiteReception />
                    </Route>
                    <Route path="/modifierUser">
                        <ModifierUser />
                    </Route>
                    <Route path="/choixUser">
                        <ChoixUser />
                    </Route>
                    <Route path="/creationCompteAdmin">
                        <CreationCompteAdmin />
                    </Route>
                    <Route path="/listePersonnes">
                        <ListePersonnes />
                    </Route>
                    <Route path="/reserverSalle">
                        <ReserverSalle />
                    </Route>
                    <Route path="/modifierSalle">
                        <ModifierSalle />
                    </Route>
                    <Route path="/choixSalle">
                        <ChoixSalle />
                    </Route>
                    <Route path="/salleComplete">
                        <SalleComplete />
                    </Route>
                    <Route path="/listeSalles">
                        <ListeSalles />
                    </Route>
                    <Route path="/creationSalle">
                        <CreationSalle />
                    </Route>
                    <Route path="/menuAdmin">
                        <MenuAdmin />
                    </Route>
                    <Route path="/menuUser">
                        <MenuUser />
                    </Route>
                    <Route path="/creationCompte">
                        <CreationCompte />
                    </Route>
                    <Route path="/">
                        <Authentification />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}