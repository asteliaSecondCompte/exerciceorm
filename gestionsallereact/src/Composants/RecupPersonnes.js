import React, { useState, useEffect, Fragment } from 'react';
import { Container } from 'react-bootstrap';

export default function RecupPersonnes() {
    const [pret, setPret] = useState(false);
    const [liste, setListe] = useState([]);

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup();
            setPret(true);
        }
        fetchJson();
        recup(setListe);
    }, []);

    return <Fragment>
        {pret ? (
            <Container>
                <table className="tableCentree enteteTableau">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Date de naissance</th>
                            <th>Actif</th>
                            <th>Mail</th>
                            <th>Adresse</th>
                            <th>Rôle</th>
                        </tr>
                    </thead>
                    {recupListe(liste)}
                </table>
            </Container>
        ) : (<div></div>)}
    </Fragment>
}

function recupListe(liste) {
    return (
        <tbody>
            {liste.map((personne) =>
                <tr key={personne.id_personne}>
                    <td>{personne.id_personne}</td>
                    <td>{personne.nom}</td>
                    <td>{personne.prenom}</td>
                    <td>{personne.dateNaissance.dayOfMonth + "/" + personne.dateNaissance.monthValue + "/" + personne.dateNaissance.year}</td>
                    <td>{"" + personne.actif}</td>
                    <td>{personne.email}</td>
                    <td>{personne.adresse}</td>
                    <td>{personne.role}</td>
                </tr>
            )}
        </tbody>
    );
}

async function recup(setListe) {
    await fetch(`${ process.env.REACT_APP_API_URL }/SVURest`)
        .then(response => response.json())
        .then(json => { setListe(json) })
        .catch(error => { console.log(error); })
}

/*
return (
        <tbody>
            <tr>
                <td>1</td>
                <td>Wayne</td>
                <td>Bruce</td>
                <td>25/12/1930</td>
                <td>true</td>
                <td>batman@gmal.com</td>
                <td>Manoir Wayne, Gotham City</td>
                <td>Formateur</td>
            </tr>
        </tbody>
    );
*/