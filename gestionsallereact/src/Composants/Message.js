import React from 'react';
import { Row, Form, Col } from 'react-bootstrap';

export default function Message(props) {

    if (props.value === "true") {
        return messageActif(props);
    }
    else if (props.value === "false") {
        return messageInactif(props);
    }
}

function messageInactif(props) {
    return (
        <li>
            <Row>
                <Col md="auto" xs="9">
                    <Form.Label><h4>{props.expediteur}</h4></Form.Label><br />
                    <Form.Label><h5>{props.objet}</h5></Form.Label><br />
                    <Form.Label><h6>{props.message}</h6></Form.Label>
                </Col>
                <Col md="auto" xs="4">
                    <Form.Label>{"A " + props.destinataire}</Form.Label><br />
                </Col>
                <Col md="auto" xs="4">
                    <Form.Label>{props.date}</Form.Label><br />
                    <Form action="/voirMessage">
                        <input id="idMessage" value={props.idMessage} hidden readOnly />
                        <input className="btn btn-success" type="submit" value="Voir" />
                    </Form>
                </Col>
            </Row>
            <hr />
        </li>
    );
}

function messageActif(props) {
    return (
        <li>
            <Row>
                <Col md="auto" xs="9">
                    <Form.Label><h4>{props.personne}</h4></Form.Label><br />
                    <Form.Label><h5>{props.objet}</h5></Form.Label><br />
                    <Form.Label><h6>{props.message}</h6></Form.Label>
                </Col>
                <Col md="auto" xs="4">
                    <Form.Label>{props.date}</Form.Label><br />
                    <Form action="/voirMessage">
                        <input id="idMessage" value={props.idMessage} hidden readOnly />
                        <input className="btn btn-success" type="submit" value="Voir" />
                    </Form>
                </Col>
                <Col md="auto" xs="4">
                    <Form action={ props.page }>
                        <input id="idMessage" value={props.idMessage} hidden readOnly /><br/>
                        <input className="btn btn-warning" type="submit" value="Archiver" />
                    </Form>
                </Col>
            </Row>
            <hr />
        </li>
    );
}