import React from 'react';
import { Link } from 'react-router-dom';

export default function MenuSalles() {
    return (
        <header>
            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <Link className="nav-link" to="/creationSalle">Créer une salle</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/choixSalle">Modifier une salle</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/listeSalles">Lister les salles</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/listePersonnes">Gestion Utilisateurs</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/boiteReception">Messagerie</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link deconnexion" to="/">Déconnexion</Link>
                </li>
            </ul>
        </header>
    );
}
