import React from 'react';
import { Link } from 'react-router-dom';

export default function MenuMessagerie() {
    return (
        <header>
            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <Link className="nav-link" to="/nouveauMessage">Nouveau Message</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/boiteReception">Boîte de réception</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/messagesEnvoyes">Messages envoyés</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/messagesArchives">Messages archivés</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/listeSalles">Gestion des salles</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/listePersonnes">Gestion Utilisateurs</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link deconnexion" to="/">Déconnexion</Link>
                </li>
            </ul>
        </header>
    );
}
