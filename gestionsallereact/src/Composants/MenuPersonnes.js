import React from 'react';
import { Link } from 'react-router-dom';

export default function MenuPersonnes() {
    return (
        <header>
            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <Link className="nav-link" to="/creationCompteAdmin">Créer un compte</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/choixUser">Modifier un utilisateur</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/listePersonnes">Liste des personnes</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/listeSalles">Gestion des salles</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/boiteReception">Messagerie</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link deconnexion" to="/">Déconnexion</Link>
                </li>
            </ul>
        </header>
    );
}
