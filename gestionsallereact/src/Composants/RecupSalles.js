import React, { useState, useEffect, Fragment } from 'react';
import { Container } from 'react-bootstrap';

export default function RecupSalles() {
    const [pret, setPret] = useState(false);
    const [liste, setListe] = useState([]);

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup();
            setPret(true);
        }
        fetchJson();
        recup(setListe);
    }, []);


    return <Fragment>
        {pret ? (
            <Container>
                <table className="tableCentree">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Numéro</th>
                            <th>Type de salle</th>
                            <th>Nom</th>
                            <th>Capacité</th>
                            <th>Surface</th>
                            <th>Bâtiment</th>
                            <th></th>
                        </tr>
                    </thead>
                    {recupListe(liste)}
                </table>
            </Container>
        ) : (<div></div>)
        }
    </Fragment >
}

function recupListe(liste) {
    return (
        <tbody>
            {liste.map((salle) =>
                <tr key={salle.id_salle}>
                    <td>{salle.id}</td>
                    <td>{salle.numero}</td>
                    <td>{salle.typeSalle}</td>
                    <td>{salle.nom}</td>
                    <td>{salle.capacite}</td>
                    <td>{salle.surface}</td>
                    <td>{salle.batiment}</td>
                    <td>
                        <form action="salleComplete">
                            <input id="id" value="1" hidden readOnly />
                            <input className="btn btn-success" type="submit" value="Voir" />
                        </form>
                    </td>
                </tr>
            )}
        </tbody>
    );
}

async function recup(setListe) {
    await fetch(`${process.env.REACT_APP_API_URL}/csRest`)
        .then(response => response.json())
        .then(json => { setListe(json) })
        .catch(error => { console.log(error); })
}