import React from 'react';
import './App.css';
import Chemin from './Chemin/Chemin';
import './Pages/Page.css'

function App() {
  return (
    <div className="App">
      <Chemin/>
    </div>
  );
}

export default App;
