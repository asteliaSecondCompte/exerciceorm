/* Exercice 1 */

/*$("#couleurs").change(function () {
    $("#photo").css({"border-color" : $(this).val()});
});

$("#largeur").click(function () {
    $("#photo").css("width", $("#dimension").val());
});

$("#hauteur").click(function () {
    $("#photo").css("height", $("#dimension").val());
});

$("#g").click(function () {
    $("#photo").css("left", $("#photo").position().left - 50);
});

$("#h").click(function () {
    $("#photo").css("top",  $("#photo").position().top - 50);
});

$("#d").click(function () {
    $("#photo").css("left",  $("#photo").position().left + 50);
});

$("#b").click(function () {
    $("#photo").css("top",  $("#photo").position().top + 50);
});*/


/* Exercice 2 */

$("#gauche").click(function() {
    $("#carre").css("left", $("#carre").position().left - 50);
});

$("#haut").click(function() {
    $("#carre").css("top", $("#carre").position().top - 50);
});

$("#droite").click(function() {
    $("#carre").css("left", $("#carre").position().left + 50);
});

$("#bas").click(function() {
    $("#carre").css("top", $("#carre").position().top + 50);
});


/* Exercice 3 */

var canevas = document.getElementById("canevas").getContext("2d");
/*canevas.beginPath();*/
canevas.lineWidth = "10";
canevas.strokeStyle="white";
canevas.rect(50, 40, 60, 50);
canevas.stroke();

/* Le rectangle n'apparait pas entièrement. */


/* Exercice 4 */

/*var cacher = document.getElementById("cacher").getContext("2d");
cacher.lineWidth = "2";
cacher.strokeStyle="white";
cacher.arc(35, 50, 5, 0, 2*Math.PI);
cacher.stroke();


cacher.lineWidth = "2";
cacher.fillRect(150, 50, 20, 20);
cacher.stroke();
*/


/* Exercice 5 */

var res = 0;
$("#addition").click(function () {
    res += parseInt($("#valeur").val());
    $("#historique").html(res);
});

$("#reset3").click(function () {
    res = 0;
    $("#valeur").val(0);
    $("#historique").html("");
});


/* Exercice 6 */

$("#calcul").click(function () {
    let salaireNet = 0;
    salaireNet = parseFloat($("#salaire").val());
    salaireNet -= salaireNet*18/100 - salaireNet*7/100 - salaireNet*5/100;
    if ("femme" == $("[name='genre']:checked").val()) {
        salaireNet += salaireNet*2/100;
    }
    if ($("#charge").val() == 3) {
        salaireNet += salaireNet*1/100;
    }
    else if ($("#charge").val() == 4) {
        salaireNet += salaireNet*2/100;
    }
    $("#impots").html("Salaire net : "+ salaireNet + "$");
});


/* Exercice 7 */

var nombreATrouver = parseInt(Math.random()*100);
var tours = 0;
$("#test").click(function() {
    if ($("#nombre").val() < nombreATrouver) {
        $("#justePrix").html("C'est plus !");
    }
    else if ($("#nombre").val() > nombreATrouver) {
        $("#justePrix").html("C'est moins !");
    }
    else {
        $("#justePrix").html("Bingo ! En "+tours+" coups.");
    }
    tours++;
});

$("#reset").click(function () {
    $("#justePrix").html("");
    $("#nombre").val("");
    nombreATrouver = parseInt(Math.random()*100);
    tours = 0;
});


/* Exercice 8 */

/* 1) */
var tab1 = [-2, 1, 4];
console.log("1) "+tab1);

/* 2) */
function add2(x) {
    return x + 2;
}
console.log(add2(5));

/* 3) */
$("#affiche").click(function() {
    $("#resultat").html("Premier : "+add2(tab1[0])+"; Dernier : "+add2(tab1[tab1.length-1]));
});


/* Exercice 9 */

$("#bJour").click(function () {
    let jour = new Date().getDay();
    switch (jour) {
        case 1 : $("#jour").html("Lundi");
            break;
        case 2 : $("#jour").html("Mardi");
            break;
        case 3 : $("#jour").html("Mercredi");
            break;
        case 4 : $("#jour").html("Jeudi");
            break;
        case 5 : $("#jour").html("Vendredi");
            break;
        case 6 : $("#jour").html("Samedi");
            break;
        default : $("#jour").html("Dimanche");
            break;
    }
});


/* Exercice 10 */

var heures = 0;
var minutes = 0;
var secondes = 0;
var millisecondes=0;
var chronoInterval;
$("#timer").val(heures+" h : "+" min : "+secondes+" s : "+millisecondes+" ms");

$("#start").click(function () {
    chronoInterval = setInterval(afficherChrono, 1);
});

var chrono = 0;
function afficherChrono() {
    millisecondes = (chrono++)%100;
    if (chrono%100 == 0) {
        secondes++;
        millisecondes = 0;
        if (secondes%60 == 0) {
            minutes++;
            secondes = 0;
            if (minutes%60 == 0) {
                heures++;
                minutes = 0;
            }
        }
    }
    $("#timer").val(heures+" h : "+minutes+" min : "+secondes+" s : "+millisecondes+" ms");

}

$("#stop").click(function () {
    clearInterval(chronoInterval);
});

$("#reset2").click(function () {
    clearInterval(chronoInterval);
    heures = 0;
    minutes = 0;
    secondes = 0;
    millisecondes=0;
    $("#timer").val(heures+" h : "+minutes+" min : "+secondes+" s : "+millisecondes+" ms");
});


/* Exercice 11 */

$("#calcul2").click(function () {
    let n = 0;
    if ( $("#n").val() > 0) {
        n = parseInt($("#n").val());
    }
    $("#cn").html(n);
    for (i=1; i<=10; i++) {
        $("#c"+i).html(n*i);
    }
});