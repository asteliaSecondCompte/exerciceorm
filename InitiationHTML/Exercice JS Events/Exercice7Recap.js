(function enregistrer() {
    let nom = localStorage.getItem("nom");
    let prenom = localStorage.getItem("prenom");
    let age = localStorage.getItem("age");
    let mail = localStorage.getItem("mail");
    if (control(nom, prenom, age, mail)) {
        $("#collecter").append(
            "<tr>"+
                "<td>"+nom+"</td>"+
                "<td>"+prenom+"</td>"+
                "<td>"+age+"</td>"+
                "<td>"+mail+"</td>"+
            "</tr>"
        );
    }
    else {
        $("#problemes").html("");
        if (!nom.match("^[a-zA-Z\\-\\s]+$")) {
            $("#problemes").append("Le format du nom est incorrect !\n");
        }
        if (!prenom.match("^[a-zA-Z\\-\\s]+$")) {
            $("#problemes").append("\nLe format du prénom est incorrect !\n");
        }
        if (!age.match("^[0-9]{1,2}||11[0-9]$")) {
            $("#problemes").append("\nLe format de l'age est incorrect !\n");
        }
        if (!mail.match("^[a-z0-9]+(\\.[a-z0-9])*@[a-z]{1,5}\\.[a-z]{1,3}$")) {
            $("#problemes").append("\nLe format du mail est incorrect !\n");
        }
    }
})();


function control(n, p, a, m) {
    let nom = n.match("^[a-zA-Z\\-\\s]+$");
    let prenom = p.match("^[a-zA-Z\\-\\s]+$");
    let age = a.match("^[0-9]{1,3}$");
    let mail = m.match("^[a-z0-9]+(\\.[a-z0-9])*@[a-z]{1,5}\\.[a-z]{1,3}$");
    return nom && prenom && age && mail;
}