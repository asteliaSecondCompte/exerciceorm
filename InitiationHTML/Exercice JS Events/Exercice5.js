$("#valider").click(function() {
    if ($("#salaire").val() < 0) {
        $("#resultat").html("Le salaire ne peut être négatif !");
    } 
    else if (!$("#codePostal").val().match("^[0-9]{5}$")) {
        $("#resultat").html("Le code postal ne possède que 5 caractères !");
    }
    else {
        calcul($("#salaire").val(), $("#codePostal").val());
    }
})


function calcul(salaire, codePostal) {
    if (codePostal.match("^59[0-9]{3}$") 
        || codePostal.match("^62[0-9]{3}$")) {
            $("#resultat").html("Vous ne payez pas d'impôts !");
    }
    else {
        $("#resultat").html("Vous devez payez "
            +(parseFloat(salaire)/2)+"€.");
    }
}