$("#valider").click(function () {
    if (control()) {
        //document.getElementById("collecter").append
        $("#collecter").append(
            "<tr>"+
                "<td>"+$("#nom").val()+"</td>"+
                "<td>"+$("#prenom").val()+"</td>"+
                "<td>"+$("#age").val()+"</td>"+
                "<td>"+$("#mail").val()+"</td>"+
            "</tr>"
        );
        $("#nom").val("");
        $("#prenom").val("");
        $("#age").val("");
        $("#mail").val("");
        $("#problemes").val("");
    }
    else {
        $("#problemes").html("");
        if (!$("#nom").val().match("^[a-zA-Z\\-\\s]+$")) {
            $("#problemes").append("Le format du nom est incorrect !\n");
        }
        if (!$("#prenom").val().match("^[a-zA-Z\\-\\s]+$")) {
            $("#problemes").append("\nLe format du prénom est incorrect !\n");
        }
        if (!$("#age").val().match("^[0-9]{1,2}||11[0-9]$")) {
            $("#problemes").append("\nLe format de l'age est incorrect !\n");
        }
        if (!$("#mail").val().match("^[a-z0-9]+(\\.[a-z0-9])*@[a-z]{1,5}\\.[a-z]{1,3}$")) {
            $("#problemes").append("\nLe format du mail est incorrect !\n");
        }
    }
})


function control() {
    let nom = $("#nom").val().match("^[a-zA-Z\\-\\s]+$");
    let prenom = $("#prenom").val().match("^[a-zA-Z\\-\\s]+$");
    let age = $("#age").val().match("^[0-9]{1,3}$");
    let mail = $("#mail").val().match("^[a-z0-9]+(\\.[a-z0-9])*@[a-z]{1,5}\\.[a-z]{1,3}$");
    return nom && prenom && age && mail;
}