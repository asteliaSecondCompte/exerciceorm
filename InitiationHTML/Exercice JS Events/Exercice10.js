
$("#contenu").change(function() {
    $("#carac").html(+$("#contenu").val().length);
    let cpt = 0;
    for (i=0; i<$("#contenu").val().length; i++) {
        if (!$("#contenu").val().charAt(i).match("\\s")) {
            cpt++;
        }
    }
    $("#mot").html(" "+cpt);
    verif();
})

function verif(cpt) {
    if ($("#contenu").val().length > 10 || cpt > 100) {
        $("#depassement").html("Dépassement");
        $("#depassement").css("color", "red");
    }
    else {
        $("#depassement").html("");
    }
}