var secret;
var tentatives = 0;
var longueur = 0;

$("#newGame").click(function() {
    secret = "";
    $("#historique").html("");
    longueur = $("[name=difficulte]:checked").val();
    for (i=1; i<longueur; i++) {
        let chiffre = Math.round(Math.random()*100)%10;
        while (unique(secret, chiffre)) {
            chiffre = Math.round(Math.random()*100)%10;
        }
        secret += chiffre;
    }
    $("#historique").append("Devine mon nombre à "+longueur+" chiffres.\n\n");
})

$("#reset").click(function() {
    if (longueur != 0) {
        $("#historique").html("");
    }
})

$("#valider").click(function() {
    let essai = $("#essai").val();
    if (essai == secret && longueur != 0) {
        $("#historique").append("\nGagner ! Le nombre est bien : "+secret+"\n");
        $("#historique").append("Nombre de coups : "+tentatives+"\n");
        longueur = 0;
    }
    else if (longueur != 0) {
        test(essai);
    }
})

$("#solution").click(function() {
    if (longueur != 0) {
        $("#historique").append("\nVous avez perdu ! La solution était : "+secret+"\n");
        longueur = 0;
    }
    
})


/**
 * Permet de verifier si l'essai est egal au nombre secret
 * @param {String} essai 
 */
function test(essai) {
    if (essai.length != longueur) {
        $("#historique").append("\nVous devez chercher un nombre "
        +"d'une longueur de "+longueur+" et non de "+essai.length+"\n");
    }
    else {
        let taureau = 0;
        let vache = 0;
        for (i=0; i<secret.length; i++) {
            if (secret.charAt(i) == essai.charAt(i)) {
                taureau++;
            }
            for (j=0; j<secret.length; j++) {
                if (secret.charAt(j) == essai.charAt(i)) {
                    vache++;
                }
            }
        }
        vache -= taureau;
        tentatives++;
        $("#historique").append(tentatives+": "+essai
            +" : "+taureau+" taureau(x) et "+vache+" vache(s)\n");
    }
}

function unique(nombre, chiffre) {
    for (i=0; i<nombre.length; i++) {
        if (nombre.charAt(i) == chiffre) {
            return true;
        }
    }
    return false;
}