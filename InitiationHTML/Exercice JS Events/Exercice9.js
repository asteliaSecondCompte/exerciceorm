var heures = (new Date()).getHours();
var minutes = (new Date()).getMinutes();
var secondes = (new Date()).getSeconds();
var chronotime = setInterval(afficherChrono, 1);

var chrono = 0;
function afficherChrono() {
    millisecondes = (chrono++)%234;
    if (chrono%234 == 0) {
        secondes++;
        millisecondes = 0;
        if (secondes%60 == 0) {
            minutes++;
            secondes = 0;
            if (minutes%60 == 0) {
                heures++;
                minutes = 0;
                if (heures%24==0) {
                    heures=0;
                }
            }
        }
    }
    $("#timer").val(heures+" h : "+minutes+" min : "+secondes+" s");
}

