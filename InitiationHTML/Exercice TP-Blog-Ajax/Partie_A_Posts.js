fetch("https://jsonplaceholder.typicode.com/posts?userId=" + localStorage.getItem("id"))
    .then(response => response.json())
    .then(json => parseListe(json))

$("#user").append(localStorage.getItem("id"));

$("#ajoutPost").html("<a href='Partie_B_ajout.html?id="+localStorage.getItem('id')+"'>Ajout Post</a>")

function parseListe(liste) {
    $("#listePosts").html("");
    for (i = 0; i < liste.length; i++) {
        $("#listePosts").append(
            "<tr>"
            + "<td><a href='Partie_A_Comm.html?postId="+liste[i].id+"'>" + liste[i].title + "</a></td>"
            + "<td>" 
                    +"<span id='td"+liste[i].id+"'>"+debut(liste[i].body)+"</span>"
                    + "<input id='aff"+liste[i].id+"' class='afficher' type='button' value='Lire la suite'/>"
                    + "<input id='cac"+liste[i].id+"' class='cacher' type='button' value='Réduire'/>"
            + "</td>"
            + "</tr>"
        );
        afficher(liste[i].id, liste[i].body);
        cacher(liste[i].id, liste[i].body);
    }
}

function cacher(nb, contenu) {
    $("#cac"+nb).click(function () {
        $("#td"+nb).html(debut(contenu));
        $(this).attr("class", "cacher");
        $("#aff"+nb).attr("class", "afficher");
    });
}

function afficher(nb, contenu) {
    $("#aff"+nb).click(function () {
        $("#td"+nb).html(contenu)
        $(this).attr("class", "cacher");
        $("#cac"+nb).attr("class", "afficher");
    });
}

function debut(contenu) {
    return contenu.split("\n", 1)[0];
}