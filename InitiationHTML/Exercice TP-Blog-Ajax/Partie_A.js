
$("#valider").click(function () {
    fetch("https://jsonplaceholder.typicode.com/users")
        .then(response => response.json())
        .then(json => parseListe(json));
})

$("#cacher").click(function () {
    $("#listeUsers").html("");
})



function parseListe(liste) {
    $("#listeUsers").html("");
    for (i = 0; i < liste.length; i++) {
        $("#listeUsers").append(
            "<tr>"
            + "<td>" + liste[i].id + "</td>"
            + "<td>" + liste[i].name + "</td>"
            + "<td>" + liste[i].username + "</td>"
            + "<td>" + liste[i].email + "</td>"
            + "<td>" + liste[i].phone + "</td>"
            + "<td>"
            + "<form id='form" + liste[i].id + "' action='Partie_A_Posts.html' target='_blank'>"
                + "<input id='post"+ liste[i].id +"' type='button' value='Liste de posts'/>"
            + "</form>"
            + "</td>"
            + "</tr>"
        );
        impEvents(i);
    }
    
}

function impEvents(nb) {
    $("#post"+nb).click(function () {
        localStorage.setItem("id", nb);
        $("#form"+nb).submit();
        console.log(nb);
    })
}