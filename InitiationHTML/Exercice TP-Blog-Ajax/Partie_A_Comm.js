fetch("https://jsonplaceholder.typicode.com/comments?"+recupURL(document.URL))
    .then(response => response.json())
    .then(json => afficherComms(json))

function afficherComms(liste) {
    $("#listeComms").html("");
    for (i = 0; i < liste.length; i++) {
        $("#listeComms").append(
            "<tr>"
            + "<td>"+liste[i].id+"</td>"
            + "<td>"+liste[i].name+"</td>"
            + "<td>"+liste[i].email+"</td>"
            + "<td>"+liste[i].body+"</td>"
            +"</tr>"
        );
    }
}

function recupURL(url) {
    return url.split("?")[1];
}

