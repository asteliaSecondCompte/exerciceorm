$("#ajout").click(function() {
    fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
            title: $("#title").val(),
            body: $("#contenu").val(),
            userId: recupURL(document.URL)
        }),
        headers: {
            "Content-type": "application/json; "
        }
    })
    .then(response => response.json())
    .then(json => console.log(json))
})

function recupURL(url) {
    return url.split("=")[1];
}