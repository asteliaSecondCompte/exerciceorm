package fr.afpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.dao.UserDAO;

@Repository
public interface UserRepository extends JpaRepository<UserDAO, Integer> {
	
	
	
}
