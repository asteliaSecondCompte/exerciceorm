package fr.afpa.dto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dao.UserDAO;
import fr.afpa.entites.Utilisateur;
import fr.afpa.interfaces.dto.IDTOUtilisateur;
import fr.afpa.repositories.UserRepository;

@Service
public class DTOUtilisateur implements IDTOUtilisateur {

	@Autowired
	private UserRepository userRepository;
	
	
	public List<Utilisateur> listeUtilisateurs() {
		return userRepository.findAll()
							.stream()
							.map(user -> userToUtilisateur(user))
							.collect(Collectors.toList());
	}
	
	
	public Utilisateur ajouterUtilisateur(Utilisateur utilisateur) {
		UserDAO user = utilisateurToUser(utilisateur);
		try {
			return userToUtilisateur(userRepository.save(user));
		} catch(Exception e) {
			return null;
		}
	}
	
	
	private UserDAO utilisateurToUser(Utilisateur utilisateur) {
		UserDAO user = new UserDAO();
		user.setNom(utilisateur.getNom());
		user.setPrenom(utilisateur.getPrenom());
		user.setEmail(utilisateur.getEmail());
		user.setTelephone(utilisateur.getTelephone());
		return user;
	}
	
	private Utilisateur userToUtilisateur(UserDAO user) {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setId(user.getId());
		utilisateur.setNom(user.getNom());
		utilisateur.setPrenom(user.getPrenom());
		utilisateur.setEmail(user.getEmail());
		utilisateur.setTelephone(user.getTelephone());
		return utilisateur;
	}
	
}
