package fr.afpa.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.entites.Utilisateur;
import fr.afpa.interfaces.dto.IDTOUtilisateur;
import fr.afpa.interfaces.services.IServicesUtilisateur;

@Service
public class ServicesUtilisateur implements IServicesUtilisateur {

	@Autowired
	private IDTOUtilisateur dtoUtilisateur;
	
	public List<Utilisateur> listeUtilisateurs() {
		return dtoUtilisateur.listeUtilisateurs();
	}
	
	public Utilisateur enregistrerUtilisateur(Utilisateur utilisateur) {
		return dtoUtilisateur.ajouterUtilisateur(utilisateur);
	}
	
}
