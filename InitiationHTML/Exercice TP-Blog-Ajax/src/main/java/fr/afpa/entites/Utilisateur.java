package fr.afpa.entites;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
@EqualsAndHashCode

@Component
public class Utilisateur {

	private int id;
	private String nom;
	private String prenom;
	private String email;
	private String telephone;
	
	public Utilisateur(String nom, String prenom, String email, String telephone) {
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
	}
	
}
