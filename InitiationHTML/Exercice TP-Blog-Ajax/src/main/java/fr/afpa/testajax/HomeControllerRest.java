package fr.afpa.testajax;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.entites.Utilisateur;
import fr.afpa.interfaces.services.IServicesUtilisateur;

/**
 * Handles requests for the application home page.
 */
@RestController
@CrossOrigin(origins = "http://localhost:5500")
public class HomeControllerRest {

	@Autowired
	private IServicesUtilisateur servicesUtilisateur;
	
	
	@GetMapping(value = "/")
	public List<Utilisateur> listeUtilisateurs() {
		return servicesUtilisateur.listeUtilisateurs();
	}
	
	@PostMapping(value = "/ajouter")
	public Utilisateur ajouterUser(@RequestParam(value = "nom") String nom,
								@RequestParam(value = "prenom") String prenom,
								@RequestParam(value = "email") String email,
								@RequestParam(value = "telephone") String telephone) {
		Utilisateur utilisateur = new Utilisateur(nom, prenom, email, telephone);
		return servicesUtilisateur.enregistrerUtilisateur(utilisateur);
	}

}
