package fr.afpa.interfaces.services;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.afpa.entites.Utilisateur;

@Service
public interface IServicesUtilisateur {

	public List<Utilisateur> listeUtilisateurs();
	
	public Utilisateur enregistrerUtilisateur(Utilisateur utilisateur);
	
}
