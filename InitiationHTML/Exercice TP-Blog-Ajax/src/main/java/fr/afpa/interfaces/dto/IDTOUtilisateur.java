package fr.afpa.interfaces.dto;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.afpa.entites.Utilisateur;

@Service
public interface IDTOUtilisateur {

	public List<Utilisateur> listeUtilisateurs();
	
	public Utilisateur ajouterUtilisateur(Utilisateur utilisateur);
	
}
