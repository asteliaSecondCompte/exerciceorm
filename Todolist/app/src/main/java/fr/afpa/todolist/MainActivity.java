package fr.afpa.todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView mTodolist;
    private TextView mRecu;
    private EditText mTodo;
    private Button mAjouter;
    private ListView listeTodos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTodolist = (TextView) findViewById(R.id.todolist);
        mTodo = (EditText) findViewById(R.id.zonetext);
        mAjouter = (Button) findViewById(R.id.ajouter);
        mRecu = (CheckBox) findViewById(R.id.recu);
        listeTodos = (ListView) findViewById(R.id.listeTodos);

        mAjouter.setEnabled(false);
        mTodo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAjouter.setEnabled(s.toString().length() > 2);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mAjouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecu.setText(mTodo.getText());
                //listeTodos.
            }
        });

    }
}
