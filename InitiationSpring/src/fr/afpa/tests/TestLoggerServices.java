package fr.afpa.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.afpa.imp.BeanFileLogger;
import fr.afpa.imp.FileLogger;
import fr.afpa.imp.SimpleCalculator;
import fr.afpa.imp.StderrLogger;
import fr.afpa.services.ICalculator;
import fr.afpa.services.ILogger;

public class TestLoggerServices {

	@Before
	public void beforeEachTest() {
		System.err.println("======================");
	}

	@After
	public void afterEachTest() {

	}

	
	// use a logger
	void use(ILogger logger) {
		logger.log("Voila le resultat = hello");
	}

	// use a calculator
	void use(ICalculator calc) {
		calc.add(100, 200);
	}
	
	// Test StderrLogger
	@Test
	public void testStderrLogger() {
		// create the service
		StderrLogger logger = new StderrLogger();

		// start the service
		logger.start();

		// use the service
		use(logger);

		// stop the service
		logger.stop();
	}

	// Test FileLogger
	@Test
	public void testFileLogger() {
		FileLogger logger = new FileLogger("test.log");
		logger.start();
		use(logger);
		logger.stop();
	}

	// Test BeanFileLogger
	@Test
	public void testBeanFileLogger() {
		// create the service
		BeanFileLogger logger = new BeanFileLogger();
		
		// set parameter
		//logger.setFileName("test.log");

		// start the service
		logger.start();

		// use the service
		use(logger);

		// stop the service
		logger.stop();
	}

	// Test SimpleCalculator
	@Test
	public void testCalculatorAndStderrLogger() {
		
		// create and start the logger service (on stderr)
		/*
		 * StderrLogger logger = new StderrLogger();
		 * logger.start();
		*/
		
		// create file logger
		BeanFileLogger logger = new BeanFileLogger();
		logger.setFileName("test.log");
		logger.start();
		
		// create, inject and start the calculator service
		SimpleCalculator calculator = new SimpleCalculator();
		calculator.setLogger(logger);
		calculator.start();
		
		// use the calculator service
		use(calculator);
		
		// stop the calculator service
		calculator.stop();
		
		// stop the logger service
		logger.stop();
	}
	
}
