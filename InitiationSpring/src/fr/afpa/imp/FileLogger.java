package fr.afpa.imp;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Date;

import fr.afpa.services.ILogger;

public class FileLogger implements ILogger {

	// parameter : the writer
	private final PrintWriter writer;
	
	
	public FileLogger(String fileName) {
		try {
			this.writer = new PrintWriter(new FileOutputStream(fileName, true));
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("bad fileName");
		}
	}
	
	
	public void start() {
		System.err.println("Start "+this);
	}
	
	public void stop() {
		writer.close();
		System.err.println("Stop "+this);
	}
	
	@Override
	public void log(String message) {
		System.err.printf("%tF %1$tR | %s\n", new Date(), message);
	}
	
	
	
}
