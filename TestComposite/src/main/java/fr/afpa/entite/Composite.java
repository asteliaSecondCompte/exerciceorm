package fr.afpa.entite;

import java.util.ArrayList;
import java.util.List;

import lombok.ToString;

@ToString
public class Composite implements Composant {

	protected List<Composant> listeComposants;
	
	public Composite() {
		super();
		this.listeComposants = new ArrayList<Composant>();
	}

	public boolean add(Composant noeud) {
		return this.listeComposants.add(noeud);
	}
	
	public boolean remove(Composant noeud) {
		return this.listeComposants.remove(noeud);
	}
	
	public Composant get(int nb) {
		if (nb < this.listeComposants.size()) {
			return this.listeComposants.get(nb);
		}
		else {
			return null;
		}
	}

	@Override
	public void affichage() {
		for (Composant composant : listeComposants) {
			composant.affichage();
		}
	}

	
	
}
