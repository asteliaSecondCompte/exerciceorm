package fr.afpa.entite;

import lombok.ToString;

@ToString
public class Feuille implements Composant {

	protected int numero; 
	
	public Feuille(int numero) {
		this.numero = numero;
	}
	
	@Override
	public void affichage() {
		System.out.println("Ceci est la feuille "+this.numero+" !");
	}

}
