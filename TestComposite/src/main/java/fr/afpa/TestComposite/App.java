package fr.afpa.TestComposite;

import fr.afpa.entite.Addition;
import fr.afpa.entite.Composite;
import fr.afpa.entite.Feuille;
import fr.afpa.entite.Nombre;

public class App {
	
	public static void main(String[] args) {
		/*
		Feuille feuille1 = new Feuille(1);
		Feuille feuille2 = new Feuille(2);
		Feuille feuille3 = new Feuille(3);
		Feuille feuille4 = new Feuille(4);
		
		Composite noeud1 = new Composite();
		Composite noeud2 = new Composite();
		Composite noeud3 = new Composite();
		Composite noeud4 = new Composite();
		
		noeud1.add(feuille1);
		noeud1.add(noeud2);
		noeud1.add(feuille2);
		
		noeud2.add(noeud3);
		noeud2.add(noeud4);
		
		noeud3.add(feuille3);
		noeud4.add(feuille4);
		
		System.out.println(noeud1);
		System.out.println();
		
		noeud1.affichage();
		*/
		
		Nombre nb5 = new Nombre(5);
		Nombre nb4 = new Nombre(4);
		Nombre nb3 = new Nombre(3);
		Addition addition1 = new Addition();
		Addition addition2 = new Addition();
		addition1.add(nb5);
		addition1.add(nb4);
		addition1.add(addition2);
		addition2.add(nb3);
		addition1.affichage();
	}
	
}
