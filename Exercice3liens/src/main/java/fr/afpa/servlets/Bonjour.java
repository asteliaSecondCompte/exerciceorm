package fr.afpa.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Bonjour
 */
public class Bonjour extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Bonjour() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("<h1>Bonjour</h1>");
		response.getWriter().append("<form action=\"EnCours\" method=\"get\">");
		response.getWriter().append("<label id='nom'>Nom :</label><input name='nomValue' type='text'/>");
		response.getWriter().append("<br/><br/>");
		response.getWriter().append("<label id='prenom'>Prénom :</label><input name='prenomValue' type=\"text\"/>");
		response.getWriter().append("<br/><br/>");
		response.getWriter().append("<input type='submit' value='Valider'>");
		response.getWriter().append("</form>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
