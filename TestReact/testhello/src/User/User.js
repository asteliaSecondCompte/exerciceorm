import React, { useState, useEffect } from 'react';

function User() {
    const [users, setUsers] = useState([]);

    const getUsers = () => {
        fetch("https://jsonplaceholder.typicode.com/users")
            .then(response => {
                return response.json();
            })
            .then(data => {
                setUsers(data);
            })
            .catch(error => {
                console.log(error);
            });
    }

    useEffect(() => {
        getUsers();
    }, []);
    const usersList = users.map((user) => <li key={user.id}>{user.email}</li>);

    return <ul>{usersList}</ul>;
}

export default User;
