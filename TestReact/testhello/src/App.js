import React from 'react';
import logo from './logo.svg';
import './App.css';
import Hello from './Hello/Hello';
import HelloWithState from './Hello/HelloWithState';
import User from './User/User'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        
          <Hello name="Bruce Wayne"/>
          <HelloWithState name="Bruce Wayne"/>
          <User/>
      </header>
    </div>
  );
}

export default App;
