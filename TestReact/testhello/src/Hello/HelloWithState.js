import React from 'react';

class HelloWithState extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {"name": props.name};
    }
    
    prenom() {
        return this.state.name.split(" ")[0];
    }

    render() {
        return (<p>Hello {this.prenom()}</p>);
    }

}

export default HelloWithState;