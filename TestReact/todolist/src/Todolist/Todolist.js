import React from 'react';
import Todo from '../Todo/Todo';

var nb = 0;

class Todolist extends React.Component {

    constructor(props) {
        super(props);

        this.displayData = [];

        this.state = {inputvalue: "", body: this.displayData};
    }
    
    updateInputValue = (evt) => {
        this.setState({inputvalue: evt.target.value});
    }

    ajouter = () => {
        this.displayData.unshift(this.state.inputvalue);
        this.setState({
            body: this.displayData,
            inputvalue: ""
        });
    }

    supprimer = (name) => {
        this.state.body.splice(this.state.body.indexOf(name), 1);
        this.setState({state: this.state});
    }

    transformation = (val) => {
        return <Todo name={val} key={nb++} todos={this.state.body} setState={this.setState.bind(this)} supprimer={this.supprimer.bind(this)}/>
    }

    render() {
        return (
            <div className="container">
                <input id="ajouter" type="text" onChange={this.updateInputValue} value={this.state.inputvalue}/>
                <input id="creer" type="button" value="Ajouter" onClick={this.ajouter} />
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th className="col-md-1"></th>
                            <th className="col-md-1"></th>
                        </tr>
                    </thead>
                    <tbody id="todolist">
                        {this.state.body.map((val) => this.transformation(val))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Todolist;

