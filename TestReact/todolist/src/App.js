import React from 'react';
import './App.css';
import Todolist from './Todolist/Todolist';
//import Form from './Form';

function App() {
  return (
    <div className="App container">
      <br/>
      <h2>Ma Todo liste</h2><br/>
      <Todolist/>
      {/*<Form/>*/}
    </div>
  );
}

export default App;
