import React from 'react';

class Todo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
                        name: props.name, 
                        etat: "afaire",
                        modif: "Modifier",
                        diagnostic : props.todos
                    };
    }

    updateInputValue = (evt) => {
        if (this.state.modif === "Valider") {
            this.setState({name: evt.target.value});
        }
    }

    fait = (evt) => {
        if (evt.target.checked) {
            this.setState({etat: "fait"});
        }
        else {
            this.setState({etat: "afaire"});
        }
        
    }

    modifier = (evt) => {
        if (evt.target.value === "Modifier") {
            this.setState({modif: "Valider"});
        }
        else {
            this.setState({modif: "Modifier"});
        }
    }

    childsupprimer = () => {
        this.props.supprimer(this.state.name);
    }

    render() {
        return(
            <tr>
                <td>
                    <input type="checkbox" onChange={this.fait}/>
                </td>
                <td>
                    <input className={this.state.etat} value={this.state.name} onChange={this.updateInputValue}/>
                </td>
                <td>
                    <input type="button" value={this.state.modif} onClick={this.modifier}/>
                    <input type="button" value="Supprimer" onClick={this.childsupprimer}/>
                </td>
            </tr>
        )
    }

}

export default Todo;

