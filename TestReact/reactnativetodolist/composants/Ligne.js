import React, { useState } from 'react';
import { Text, Button, View, CheckBox } from 'react-native';
import { Icon } from 'react-native-elements';
import styles from './Styles';

export default function Ligne({value, supprimer}) {
    const [test, setTest] = useState([]);
    const [val, setVal] = useState(false);

    return (
        <View style={styles().enLigne}>
            <CheckBox value={val}
                    onValueChange={
                        () => {
                            if (val) {
                                setTest(styles().nonbarre);
                            }
                            else {
                                setTest(styles().barre);
                            }
                            setVal(!val);
                        }
                    }
            />
            <Text style={test}>{value.item}</Text>
            <Icon name="trashcan" type="octicon" onPress={() => supprimer(value.index)} />
        </View>
    );
}
