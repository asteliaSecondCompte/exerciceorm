//import React from 'react';
import { StyleSheet } from "react-native";

export default function styles() {

    const styles = StyleSheet.create({
        container: {alignItems: "center", justifyContent: "center" },
        titre : {flex: 4, alignSelf : "center", fontSize : 30, marginTop: 130 },
        input : { width: 200, height: 40, borderColor: 'gray', borderWidth: 1 },
        enLigne : {flexDirection: "row", marginTop: 40},
        enBas : {flexDirection: "column-reverse"},
        barre: {/*width: 100,*/ textDecorationLine: "line-through"},
        nonbarre: {/*width: 100,*/ textDecorationLine: "none"},
    });

    return styles;
}


