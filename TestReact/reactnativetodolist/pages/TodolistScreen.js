import React, { useState, useEffect } from 'react';
import { View, Text, Button, TextInput, FlatList, Alert } from "react-native";
import styles from '../composants/Styles';
import Ligne from '../composants/Ligne';



export default function TodolistScreen({ navigation }) {

    const [value, setValue] = useState("");
    const [liste, setListe] = useState([]);
    const [refresh, setRefresh] = useState(true);

    const lister = (liste, setRefresh, refresh) => {
        let nb = 0;
        return (
            <FlatList
                data={liste}
                renderItem={(text) => <Ligne key={nb++}
                                                value={ text } 
                                                setRefresh={setRefresh} 
                                                refresh={refresh}
                                                supprimer={supprimer.bind(liste)}/>}
            />
        );
    }

    const ajouter = () => {
        if (value !== "" && value !== undefined && value !== null) {
            setListe(liste.concat(value))
            setValue("")
        }
    }

    const supprimer = (val) => {
        setListe(liste.filter((value, index, array) => {
            return index != val
        }))
    }

    //useEffect(setListe, []);

    return (
        <View style={styles().container}>
            <View style={styles().titre}>
                <Text>Todolist</Text>
            </View>

            <View style={styles().enLigne}>
                <TextInput style={styles().input}
                    value={value}
                    onChangeText={text => setValue(text)}
                    placeholder="Todo"
                />
                <Button title="Ajouter"
                    onPress={() => ajouter()}
                />
            </View>

            <View>
                {lister(liste, setRefresh, refresh)}
                
            </View>
            
            {/*<View style={styles().enBas}>
                <Button 
                    title="Go to Home"
                    onPress={() => navigation.navigate('Home')}
                />
            </View>*/}

        </View>
    );
}
