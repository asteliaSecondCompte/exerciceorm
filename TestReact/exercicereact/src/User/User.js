import React, { useState, useEffect } from "react";

function User() {
    const [users, setUsers] = useState([]);

    const getUsers = () => { temp(setUsers) }

    useEffect(
        () => { getUsers();} , []
    );
    
    const usersList = users.map((user) => 
            <tr key={user.id}>
                <td>{user.name}</td>
                <td>
                    <a href={`/posts?userId=${ user.id }`}>{user.email}</a>
                </td>
                <td>{user.phone}</td>
            </tr>
    );

    return (<div className="container">
                <br/>
                <h2>Liste d'utilisateurs</h2>
                <table>
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Téléphone</th>
                        </tr>
                    </thead>
                    <tbody>
                        {usersList}
                    </tbody>
                </table>
            </div>
    )
}

function temp(setUsers) {
    fetch(`${process.env.REACT_APP_MOCK_URL}/users`)
        .then(
            response => { return response.json(); }
        )
        .then(
            json => { setUsers(json); }
        )
        .catch(
            error => { console.log(error); }
        )
}

export default User;