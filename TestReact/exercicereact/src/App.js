import React from 'react';
import './App.css';
import Chemin from './Router/Chemin';

import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    return (
        
        <div className="App">
            <Chemin/>
        </div>
    );
}

export default App;
