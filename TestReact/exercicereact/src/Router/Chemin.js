import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import User from "../User/User";
import Posts from "../Posts/Posts";
import Comms from "../Comm/Comm";
import FormPost from "../FormPost/FormPost";

export default function Chemin() {
    return (
        <Router>
            {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
            <Switch>
                <Route path="/posts/ajout">
                    <FormPost/>
                </Route>
                <Route path="/posts">
                    <Posts />
                </Route>
                <Route path="/comments">
                    <Comms/>
                </Route>
                <Route path="/">
                    <User />
                </Route>
            </Switch>

        </Router>
    );
}


