import React, { useState, useEffect } from "react";

function Comms() {
    const [comms, setComms] = useState([]);

    const getComms = () => { temp(setComms, document.URL) }

    useEffect(
        () => { getComms();} , []
    );

    const commsList = comms.map((comm) =>
            <tr key={comm.id}>
                <td>{comm.name}</td>
                <td>{comm.email}</td>
                <td>{comm.body}</td>
            </tr>
    );

    return (<div className="container">
        <br/>
        <h2>Liste de commentaires du { ` ${ document.URL.split("?")[1] }` }</h2>
        <table>
            <thead>
                <tr>
                    <th>Objet</th>
                    <th>Email</th>
                    <th>Contenu</th>
                </tr>
            </thead>
            <tbody>
                {commsList}
            </tbody>
        </table>
        <br/>
        <a href="/users">Liste utilisateurs</a>
        </div>
    )

}

function temp(setComms, url) {
    let postId = url.split("?")[1];
    fetch(`${process.env.REACT_APP_MOCK_URL}/comments?${postId}`)
        .then(
            response => { return response.json(); }
        )
        .then(
            json => { setComms(json); }
        )
        .catch(
            error => { console.log(error); }
        )
}

export default Comms;