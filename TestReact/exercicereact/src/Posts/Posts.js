import React, { useState, useEffect } from "react";

function Posts() {
    const [posts, setPosts] = useState([]);

    const getPosts = () => { temp(setPosts, document.URL) }

    useEffect(
        () => { getPosts();} , []
    );

    const postsList = posts.map((post) =>
        
            <tr key={post.id}>
                <td>
                    <a href={`/comments?postId=${ post.id }`}>{post.title}</a>
                </td>
                <td>{ post.body }</td>
            </tr>
    );

    return (<div className="container">
            <br/>
            <h2>Liste de posts de { ` ${ document.URL.split("?")[1] }` }</h2>
                <table>
                    <thead>
                        <tr>
                            <th>Titre</th>
                            <th>Contenu</th>
                        </tr>
                    </thead>
                    <tbody>
                        {postsList}
                    </tbody>
                </table>
                <br/>
                <a href={"/posts/ajout?"+document.URL.split("?")[1]}>Créer Post</a>
                <a href="/users">Retour</a>
            </div>
    )
}

function temp(setPosts, url) {
    let userId = url.split("?")[1];
    fetch(`${process.env.REACT_APP_MOCK_URL}/posts?${userId}`)
        .then(
            response => { return response.json(); }
        )
        .then(
            json => { setPosts(json); }
        )
        .catch(
            error => { console.log(error); }
        )
}

export default Posts;