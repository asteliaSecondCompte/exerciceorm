import React from 'react';

class FormPost extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "",
            body: ""
        }
    }

    titre = (evt) => {
        this.setState({ title: evt.target.value});
    }

    contenu = (evt) => {
        this.setState({body: evt.target.value});
    }

    temp = () => {
        let userId = document.URL.split("=")[1];
        fetch(`${process.env.REACT_APP_MOCK_URL}/posts`, {
            method: 'post',
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Accept: 'application/json'
                
            },
            body: `{
                    "title": "${this.state.title}",
                    "body": "${this.state.body}",
                    "userId": "${userId}"
                }`
        })
            .then(
                response => { return response.json(); }
            )
            .catch(
                console.log(`${process.env.REACT_APP_MOCK_URL}/posts`),
                error => { console.log(error); }
            )
    }

    render() {
        return (
        <div className="container">
            <br/><h4>Création d'un post</h4><br/>
        <form className="form-group">
            <div className="input-group">
                <div className="input-group-prepend">
                    <label className="input-group-text" htmlFor="title">Title</label>
                </div>
                <input className="form-control" id="title" type="text" onChange={this.titre}/>
            </div>
            <br/>
            <div className="input-group">
                <div className="input-group-prepend">
                    <label className="input-group-text" htmlFor="contenu">Contenu</label>
                </div>
                    <textarea className="form-control" id="contenu" onChange={this.contenu}></textarea>
            </div>
            <br/>
                {/*<a href={process.env.REACT_APP_MOCK_URL + "/posts?" + document.URL.split("?")[1]}>*/}
                    <input className="btn btn-primary" type="button" value="Créer" onClick={this.temp}/>
                {/*</a>*/}
            
            <a href="/users">Liste d'utilisateurs</a>
        </form>
        </div>
        );
    }
}

export default FormPost;



