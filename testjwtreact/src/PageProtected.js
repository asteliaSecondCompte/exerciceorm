import React, { useState, useEffect } from 'react';

export default function PageProtected() {

    const [messageserveur, setMessageServeur] = useState([]);

    const getMessageServeur = async () => {
        let reponse = await fetch(`${ process.env.REACT_APP_API_URL }/pageprotected`, {
            method: "get",
            headers: {
                "Authorization": sessionStorage.token
            }
        }).catch(error => console.log(error));

        if (reponse && (reponse.status===200 || reponse.status===401)) {
            let responseData = await reponse.json();
            setMessageServeur(responseData);
        }
    }

    useEffect( () => {
        getMessageServeur();
    }, [])

    return (
        <div>
            <br/>
            Page protégée :sourire:
            <br/><br/>
            Message envoyé par le back : { messageserveur.message }
        </div>
    )

}