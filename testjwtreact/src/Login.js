import React, { useState } from 'react';
import { useToasts } from 'react-toast-notifications';

export default function Login() {

    const [login, setLogin] = useState("");
    const [mdp, setMdp] = useState("");
    const [erreur, setErreur] = useState("");
    const {addToast} = useToasts();

    const handleLogin = (evt) => {
        setLogin(evt.target.value);
    }

    const handleMdp = (evt) => {
        setMdp(evt.target.value);
    }

    const envoie = async () => {
        let reponse = await fetch(`${process.env.REACT_APP_API_URL}/authentification`, {
            method: "post",
            headers: {
                "Accept": "application/json",
                "Content-type": "application/json; charset=UTF-8"
            },
            body: `{"login": "${login}", "mdp": "${mdp}" }`
        })
            .catch(error => console.log(error))

        sessionStorage.clear();
        if (reponse && reponse.status === 200) {
            let token = await reponse.json();
            sessionStorage.setItem("token", token.token);
            addToast("authentification réussie", { appearance: "success", autoDismiss: true });
            document.location.href = "/protectedpage";
        }
        else {
            if (reponse && reponse.status === 400) {
                setErreur("Erreur login et/ou mot de passe incorrrect(s) ! ")
                addToast("erreur authentification", { appearance: "error", autoDismiss: true });
            }
            else {
                setErreur("Serveur non disponible !")
            }
        }
    }



    return (
        <div>
            <br />
            <input type="text" onChange={handleLogin} placeholder="Login" />
            <br />
            <input type="password" onChange={handleMdp} placeholder="Mot de passe" />
            <br />
            <input type="button" onClick={envoie} value="Connexion" />
        </div>
    );

}