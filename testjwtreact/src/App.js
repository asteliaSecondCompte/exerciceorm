import React from 'react';
import './App.css';
import { Switch, Route, 
  BrowserRouter as Router } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import PageProtected from './PageProtected';
import Login from './Login';
import { ToastProvider } from 'react-toast-notifications';

function App() {
  return (
    <div className="App">
      <ToastProvider>
        <Router>
          <Switch>
            <PrivateRoute path="/protectedpage">
              <PageProtected />
            </PrivateRoute>

            <Route path="/">
              <Login />
            </Route>

          </Switch>
        </Router>
      </ToastProvider>
    </div>
  );
}

export default App;
