--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: archivage(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.archivage() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		insert into archive(id_archive, nom, prenom, datenaissance, mail, adresse, id_role, id_type_profil) values (nextval('archive_seq'),old.nom, old.prenom, old. datenaissance, old.mail, old.adresse, old.id_role, old.id_type_profil);
		return old;
	end
$$;


ALTER FUNCTION public.archivage() OWNER TO postgres;

--
-- Name: creersalle(); Type: FUNCTION; Schema: public; Owner: ndjs
--

CREATE FUNCTION public.creersalle() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		insert into materiel(id, quantite, salle, typemateriel) values(nextval('materiel_seq'), 0, currval('salle_seq'), 1);
		insert into materiel(id, quantite, salle, typemateriel) values(nextval('materiel_seq'), 0, currval('salle_seq'), 2);
		insert into materiel(id, quantite, salle, typemateriel) values(nextval('materiel_seq'), 0, currval('salle_seq'), 3);
		return new;
	end;
$$;


ALTER FUNCTION public.creersalle() OWNER TO ndjs;

--
-- Name: messageactivation(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.messageactivation() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		if (select id_type_profil from login natural join profil where login=new.login) = 2 then
			insert into message values (nextval('message_seq'), 'Activation compte', 'Demande d activation de compte.', now(), false);
			insert into login_message values ('admin', currval('message_seq'), false);	
			insert into login_message values (new.login, currval('message_seq'), true);
		end if;
		return new;
	end
$$;


ALTER FUNCTION public.messageactivation() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: archive; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.archive (
    id_archive integer NOT NULL,
    nom character varying,
    prenom character varying,
    datenaissance date,
    mail character varying(255),
    adresse character varying,
    id_role integer,
    id_type_profil integer
);


ALTER TABLE public.archive OWNER TO postgres;

--
-- Name: archive_salle_seq; Type: SEQUENCE; Schema: public; Owner: ndjs
--

CREATE SEQUENCE public.archive_salle_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.archive_salle_seq OWNER TO ndjs;

--
-- Name: archive_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.archive_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.archive_seq OWNER TO postgres;

--
-- Name: archivesalle; Type: TABLE; Schema: public; Owner: ndjs
--

CREATE TABLE public.archivesalle (
    id integer NOT NULL,
    capacite integer,
    nom character varying(255),
    numero character varying(255),
    surface real,
    batiment integer,
    typesalle integer
);


ALTER TABLE public.archivesalle OWNER TO ndjs;

--
-- Name: batiment; Type: TABLE; Schema: public; Owner: ndjs
--

CREATE TABLE public.batiment (
    id integer NOT NULL,
    nom character varying(255),
    id_centre integer
);


ALTER TABLE public.batiment OWNER TO ndjs;

--
-- Name: batiment_seq; Type: SEQUENCE; Schema: public; Owner: ndjs
--

CREATE SEQUENCE public.batiment_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.batiment_seq OWNER TO ndjs;

--
-- Name: centre; Type: TABLE; Schema: public; Owner: ndjs
--

CREATE TABLE public.centre (
    id_centre integer NOT NULL,
    nom character varying(255)
);


ALTER TABLE public.centre OWNER TO ndjs;

--
-- Name: centre_seq; Type: SEQUENCE; Schema: public; Owner: ndjs
--

CREATE SEQUENCE public.centre_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.centre_seq OWNER TO ndjs;

--
-- Name: login; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.login (
    id_profil integer NOT NULL,
    login character varying NOT NULL,
    motdepasse character varying NOT NULL
);


ALTER TABLE public.login OWNER TO postgres;

--
-- Name: login_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.login_message (
    login character varying NOT NULL,
    id_message integer NOT NULL,
    expediteur boolean NOT NULL
);


ALTER TABLE public.login_message OWNER TO postgres;

--
-- Name: materiel; Type: TABLE; Schema: public; Owner: ndjs
--

CREATE TABLE public.materiel (
    id integer NOT NULL,
    quantite integer,
    salle integer,
    typemateriel integer
);


ALTER TABLE public.materiel OWNER TO ndjs;

--
-- Name: materiel_seq; Type: SEQUENCE; Schema: public; Owner: ndjs
--

CREATE SEQUENCE public.materiel_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.materiel_seq OWNER TO ndjs;

--
-- Name: message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.message (
    id_message integer NOT NULL,
    objet character varying(50) NOT NULL,
    contenu character varying(255),
    date timestamp without time zone NOT NULL,
    archive boolean NOT NULL
);


ALTER TABLE public.message OWNER TO postgres;

--
-- Name: message_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.message_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.message_seq OWNER TO postgres;

--
-- Name: profil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profil (
    id_profil integer NOT NULL,
    nom character varying,
    prenom character varying,
    datenaissance date,
    actif boolean NOT NULL,
    mail character varying(255),
    adresse character varying,
    id_role integer NOT NULL,
    id_type_profil integer NOT NULL
);


ALTER TABLE public.profil OWNER TO postgres;

--
-- Name: profil_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.profil_seq
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profil_seq OWNER TO postgres;

--
-- Name: reservation; Type: TABLE; Schema: public; Owner: ndjs
--

CREATE TABLE public.reservation (
    id integer NOT NULL,
    datedebut date,
    datefin date,
    intitule character varying(255),
    salle integer
);


ALTER TABLE public.reservation OWNER TO ndjs;

--
-- Name: reservation_seq; Type: SEQUENCE; Schema: public; Owner: ndjs
--

CREATE SEQUENCE public.reservation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reservation_seq OWNER TO ndjs;

--
-- Name: role_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_seq OWNER TO postgres;

--
-- Name: roleprofil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roleprofil (
    id_role integer NOT NULL,
    libelle character varying
);


ALTER TABLE public.roleprofil OWNER TO postgres;

--
-- Name: salle; Type: TABLE; Schema: public; Owner: ndjs
--

CREATE TABLE public.salle (
    id integer NOT NULL,
    capacite integer,
    nom character varying(255),
    numero character varying(255),
    surface real,
    batiment integer,
    typesalle integer
);


ALTER TABLE public.salle OWNER TO ndjs;

--
-- Name: salle_seq; Type: SEQUENCE; Schema: public; Owner: ndjs
--

CREATE SEQUENCE public.salle_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.salle_seq OWNER TO ndjs;

--
-- Name: type_materiel_seq; Type: SEQUENCE; Schema: public; Owner: ndjs
--

CREATE SEQUENCE public.type_materiel_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_materiel_seq OWNER TO ndjs;

--
-- Name: type_profil_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.type_profil_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_profil_seq OWNER TO postgres;

--
-- Name: type_salle_seq; Type: SEQUENCE; Schema: public; Owner: ndjs
--

CREATE SEQUENCE public.type_salle_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_salle_seq OWNER TO ndjs;

--
-- Name: typemateriel; Type: TABLE; Schema: public; Owner: ndjs
--

CREATE TABLE public.typemateriel (
    id_typemateriel integer NOT NULL,
    type character varying(255)
);


ALTER TABLE public.typemateriel OWNER TO ndjs;

--
-- Name: typeprofil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.typeprofil (
    id_type_profil integer NOT NULL,
    libelle character varying
);


ALTER TABLE public.typeprofil OWNER TO postgres;

--
-- Name: typesalle; Type: TABLE; Schema: public; Owner: ndjs
--

CREATE TABLE public.typesalle (
    id integer NOT NULL,
    type character varying(255)
);


ALTER TABLE public.typesalle OWNER TO ndjs;

--
-- Data for Name: archive; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.archive (id_archive, nom, prenom, datenaissance, mail, adresse, id_role, id_type_profil) FROM stdin;
\.


--
-- Data for Name: archivesalle; Type: TABLE DATA; Schema: public; Owner: ndjs
--

COPY public.archivesalle (id, capacite, nom, numero, surface, batiment, typesalle) FROM stdin;
\.


--
-- Data for Name: batiment; Type: TABLE DATA; Schema: public; Owner: ndjs
--

COPY public.batiment (id, nom, id_centre) FROM stdin;
2	Extension 1	\N
3	Extension2	\N
4	Hall	\N
5	Gymnase	\N
6	Chirurgie Dentaire	\N
1	Delavalleepoussin	\N
\.


--
-- Data for Name: centre; Type: TABLE DATA; Schema: public; Owner: ndjs
--

COPY public.centre (id_centre, nom) FROM stdin;
\.


--
-- Data for Name: login; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.login (id_profil, login, motdepasse) FROM stdin;
1	admin	admin
2	nightwing	rick
\.


--
-- Data for Name: login_message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.login_message (login, id_message, expediteur) FROM stdin;
admin	1	f
nightwing	1	t
nightwing	2	f
admin	2	t
admin	3	f
nightwing	3	f
admin	3	t
\.


--
-- Data for Name: materiel; Type: TABLE DATA; Schema: public; Owner: ndjs
--

COPY public.materiel (id, quantite, salle, typemateriel) FROM stdin;
\.


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.message (id_message, objet, contenu, date, archive) FROM stdin;
1	Activation compte	Demande d activation de compte.	2020-02-03 21:09:00.524747	t
2	toto	Lalala\t	2020-02-03 21:48:39.602	f
3	le credo	Nihils versus omnia licita\t\t	2020-02-03 21:50:12.085	f
\.


--
-- Data for Name: profil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.profil (id_profil, nom, prenom, datenaissance, actif, mail, adresse, id_role, id_type_profil) FROM stdin;
1	Gali	Seti	2020-01-17	t	sg@s.g	labas	1	1
2	Grayson	Richard	1944-06-15	f	nightwing@gmal.com	Manoir Wayne	2	2
\.


--
-- Data for Name: reservation; Type: TABLE DATA; Schema: public; Owner: ndjs
--

COPY public.reservation (id, datedebut, datefin, intitule, salle) FROM stdin;
1	1999-12-31	2000-01-10	Test	1
2	2001-01-19	2001-01-29	Test sequence	1
\.


--
-- Data for Name: roleprofil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roleprofil (id_role, libelle) FROM stdin;
1	Formateur
2	Stagiaire
\.


--
-- Data for Name: salle; Type: TABLE DATA; Schema: public; Owner: ndjs
--

COPY public.salle (id, capacite, nom, numero, surface, batiment, typesalle) FROM stdin;
1	12	Salle de Ceremonie	12	12	4	4
\.


--
-- Data for Name: typemateriel; Type: TABLE DATA; Schema: public; Owner: ndjs
--

COPY public.typemateriel (id_typemateriel, type) FROM stdin;
\.


--
-- Data for Name: typeprofil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.typeprofil (id_type_profil, libelle) FROM stdin;
1	Administrateur
2	Utilisateur
\.


--
-- Data for Name: typesalle; Type: TABLE DATA; Schema: public; Owner: ndjs
--

COPY public.typesalle (id, type) FROM stdin;
3	infirmerie
2	formation
1	bureau
4	reunion
\.


--
-- Name: archive_salle_seq; Type: SEQUENCE SET; Schema: public; Owner: ndjs
--

SELECT pg_catalog.setval('public.archive_salle_seq', 1, false);


--
-- Name: archive_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.archive_seq', 1, false);


--
-- Name: batiment_seq; Type: SEQUENCE SET; Schema: public; Owner: ndjs
--

SELECT pg_catalog.setval('public.batiment_seq', 1, false);


--
-- Name: centre_seq; Type: SEQUENCE SET; Schema: public; Owner: ndjs
--

SELECT pg_catalog.setval('public.centre_seq', 1, false);


--
-- Name: materiel_seq; Type: SEQUENCE SET; Schema: public; Owner: ndjs
--

SELECT pg_catalog.setval('public.materiel_seq', 1, true);


--
-- Name: message_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.message_seq', 3, true);


--
-- Name: profil_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.profil_seq', 2, true);


--
-- Name: reservation_seq; Type: SEQUENCE SET; Schema: public; Owner: ndjs
--

SELECT pg_catalog.setval('public.reservation_seq', 2, true);


--
-- Name: role_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_seq', 3, false);


--
-- Name: salle_seq; Type: SEQUENCE SET; Schema: public; Owner: ndjs
--

SELECT pg_catalog.setval('public.salle_seq', 101, true);


--
-- Name: type_materiel_seq; Type: SEQUENCE SET; Schema: public; Owner: ndjs
--

SELECT pg_catalog.setval('public.type_materiel_seq', 1, false);


--
-- Name: type_profil_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.type_profil_seq', 3, false);


--
-- Name: type_salle_seq; Type: SEQUENCE SET; Schema: public; Owner: ndjs
--

SELECT pg_catalog.setval('public.type_salle_seq', 1, false);


--
-- Name: archive archive_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.archive
    ADD CONSTRAINT archive_pk PRIMARY KEY (id_archive);


--
-- Name: archivesalle archivesalle_pkey; Type: CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.archivesalle
    ADD CONSTRAINT archivesalle_pkey PRIMARY KEY (id);


--
-- Name: batiment batiment_pkey; Type: CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.batiment
    ADD CONSTRAINT batiment_pkey PRIMARY KEY (id);


--
-- Name: centre centre_pkey; Type: CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.centre
    ADD CONSTRAINT centre_pkey PRIMARY KEY (id_centre);


--
-- Name: login log_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login
    ADD CONSTRAINT log_pk PRIMARY KEY (login);


--
-- Name: login_message login_message_login_id_message_expediteur_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login_message
    ADD CONSTRAINT login_message_login_id_message_expediteur_key UNIQUE (login, id_message, expediteur);


--
-- Name: materiel materiel_pkey; Type: CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.materiel
    ADD CONSTRAINT materiel_pkey PRIMARY KEY (id);


--
-- Name: message message_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pk PRIMARY KEY (id_message);


--
-- Name: profil profil_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profil
    ADD CONSTRAINT profil_pk PRIMARY KEY (id_profil);


--
-- Name: reservation reservation_pkey; Type: CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (id);


--
-- Name: roleprofil roleprofil_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roleprofil
    ADD CONSTRAINT roleprofil_pk PRIMARY KEY (id_role);


--
-- Name: salle salle_pkey; Type: CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.salle
    ADD CONSTRAINT salle_pkey PRIMARY KEY (id);


--
-- Name: typemateriel typemateriel_pkey; Type: CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.typemateriel
    ADD CONSTRAINT typemateriel_pkey PRIMARY KEY (id_typemateriel);


--
-- Name: typeprofil typeprofil_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.typeprofil
    ADD CONSTRAINT typeprofil_pk PRIMARY KEY (id_type_profil);


--
-- Name: typesalle typesalle_pkey; Type: CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.typesalle
    ADD CONSTRAINT typesalle_pkey PRIMARY KEY (id);


--
-- Name: login activation; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER activation AFTER INSERT ON public.login FOR EACH ROW EXECUTE FUNCTION public.messageactivation();


--
-- Name: profil archiver; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER archiver BEFORE DELETE ON public.profil FOR EACH ROW EXECUTE FUNCTION public.archivage();


--
-- Name: salle creer; Type: TRIGGER; Schema: public; Owner: ndjs
--

CREATE TRIGGER creer AFTER INSERT ON public.salle FOR EACH STATEMENT EXECUTE FUNCTION public.creersalle();


--
-- Name: archive archive_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.archive
    ADD CONSTRAINT archive_fk FOREIGN KEY (id_type_profil) REFERENCES public.typeprofil(id_type_profil);


--
-- Name: archive archive_fk2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.archive
    ADD CONSTRAINT archive_fk2 FOREIGN KEY (id_role) REFERENCES public.roleprofil(id_role);


--
-- Name: archivesalle fk4r7hs1l91rb04dbxtvdg9uf4h; Type: FK CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.archivesalle
    ADD CONSTRAINT fk4r7hs1l91rb04dbxtvdg9uf4h FOREIGN KEY (typesalle) REFERENCES public.typesalle(id);


--
-- Name: materiel fkbaifl0s4vty52bq54ndq3uehb; Type: FK CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.materiel
    ADD CONSTRAINT fkbaifl0s4vty52bq54ndq3uehb FOREIGN KEY (salle) REFERENCES public.salle(id);


--
-- Name: salle fkfr1waearfrvkcm8oep4um4tts; Type: FK CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.salle
    ADD CONSTRAINT fkfr1waearfrvkcm8oep4um4tts FOREIGN KEY (typesalle) REFERENCES public.typesalle(id);


--
-- Name: archivesalle fkgqclqesju4hjxpd0pqg44gh19; Type: FK CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.archivesalle
    ADD CONSTRAINT fkgqclqesju4hjxpd0pqg44gh19 FOREIGN KEY (batiment) REFERENCES public.batiment(id);


--
-- Name: materiel fkixldgy2x5lm087lx578sb94ki; Type: FK CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.materiel
    ADD CONSTRAINT fkixldgy2x5lm087lx578sb94ki FOREIGN KEY (typemateriel) REFERENCES public.typemateriel(id_typemateriel);


--
-- Name: batiment fkk7yc07jk8qt9vycen2mhk7sj8; Type: FK CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.batiment
    ADD CONSTRAINT fkk7yc07jk8qt9vycen2mhk7sj8 FOREIGN KEY (id_centre) REFERENCES public.centre(id_centre);


--
-- Name: reservation fkkj56eakevlw30gnwq6750wkq1; Type: FK CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT fkkj56eakevlw30gnwq6750wkq1 FOREIGN KEY (salle) REFERENCES public.salle(id);


--
-- Name: salle fkr4ah37yj6kgk0d97h5a4dsf37; Type: FK CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.salle
    ADD CONSTRAINT fkr4ah37yj6kgk0d97h5a4dsf37 FOREIGN KEY (batiment) REFERENCES public.batiment(id);


--
-- Name: login log_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login
    ADD CONSTRAINT log_fk FOREIGN KEY (id_profil) REFERENCES public.profil(id_profil);


--
-- Name: login_message login_message_fk1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login_message
    ADD CONSTRAINT login_message_fk1 FOREIGN KEY (login) REFERENCES public.login(login);


--
-- Name: login_message login_message_fk2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login_message
    ADD CONSTRAINT login_message_fk2 FOREIGN KEY (id_message) REFERENCES public.message(id_message);


--
-- Name: profil profil_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profil
    ADD CONSTRAINT profil_fk FOREIGN KEY (id_role) REFERENCES public.roleprofil(id_role);


--
-- Name: profil profil_fk2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profil
    ADD CONSTRAINT profil_fk2 FOREIGN KEY (id_type_profil) REFERENCES public.typeprofil(id_type_profil);


--
-- PostgreSQL database dump complete
--

