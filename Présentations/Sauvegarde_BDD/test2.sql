--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: materiel; Type: TABLE; Schema: public; Owner: ndjs
--

CREATE TABLE public.materiel (
    id integer NOT NULL,
    quantite integer,
    salle integer,
    typemateriel integer
);


ALTER TABLE public.materiel OWNER TO ndjs;

--
-- Data for Name: materiel; Type: TABLE DATA; Schema: public; Owner: ndjs
--

COPY public.materiel (id, quantite, salle, typemateriel) FROM stdin;
\.


--
-- Name: materiel materiel_pkey; Type: CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.materiel
    ADD CONSTRAINT materiel_pkey PRIMARY KEY (id);


--
-- Name: materiel fkbaifl0s4vty52bq54ndq3uehb; Type: FK CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.materiel
    ADD CONSTRAINT fkbaifl0s4vty52bq54ndq3uehb FOREIGN KEY (salle) REFERENCES public.salle(id);


--
-- Name: materiel fkixldgy2x5lm087lx578sb94ki; Type: FK CONSTRAINT; Schema: public; Owner: ndjs
--

ALTER TABLE ONLY public.materiel
    ADD CONSTRAINT fkixldgy2x5lm087lx578sb94ki FOREIGN KEY (typemateriel) REFERENCES public.typemateriel(id_typemateriel);


--
-- PostgreSQL database dump complete
--

