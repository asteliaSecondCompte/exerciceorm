--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: affichagenbvols(integer); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.affichagenbvols(integer)
    LANGUAGE plpgsql
    AS $_$
		begin
			raise notice 'Nombres de vols  : %',nbvols($1);
			raise notice '';
		end
	$_$;


ALTER PROCEDURE public.affichagenbvols(integer) OWNER TO postgres;

--
-- Name: avionscroissant(); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.avionscroissant()
    LANGUAGE plpgsql
    AS $$
		declare 
			cible record;
		begin
			for cible in select * from "Avion" a order by a."Nom" asc
			loop
				raise notice '%',cible;
			end loop;
			raise notice '';
		end
	$$;


ALTER PROCEDURE public.avionscroissant() OWNER TO postgres;

--
-- Name: existe(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.existe() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
		declare
			nb int;
		begin
			select count(*) from "Avion" a where a."Nom"=new."Nom" into nb;
			if (nb > 1) then
				raise exception 'Un avion avec ce nom existe deja !';
			end if;
			return new;
		end
	$$;


ALTER FUNCTION public.existe() OWNER TO postgres;

--
-- Name: modifcap(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.modifcap() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
		declare
			nb int;
		begin
			if (new."Capacite" <> old."Capacite") then
				raise exception 'Changement de capacite impossible !';
			end if;
			return new;
		end
	$$;


ALTER FUNCTION public.modifcap() OWNER TO postgres;

--
-- Name: nbavions(); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.nbavions()
    LANGUAGE plpgsql
    AS $$
		declare 
			res int;
		begin
			select count(*) from "Avion" a into res;
			raise notice 'Nombres avions  : %',res;
			raise notice '';
		end
	$$;


ALTER PROCEDURE public.nbavions() OWNER TO postgres;

--
-- Name: nbvols(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.nbvols(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
		declare
			res int;
		begin
			select count(*) from "Avion" a natural join "Vol" v where a."NA"=$1 into res;
			return res;
		end
	$_$;


ALTER FUNCTION public.nbvols(integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Avion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Avion" (
    "NA" integer NOT NULL,
    "Nom" character varying(12) NOT NULL,
    "Capacite" integer NOT NULL,
    "Localite" character varying(10)
);


ALTER TABLE public."Avion" OWNER TO postgres;

--
-- Name: Pilote; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Pilote" (
    "NP" integer NOT NULL,
    "Nom" character varying(25) NOT NULL,
    "Adresse" character varying(40) NOT NULL
);


ALTER TABLE public."Pilote" OWNER TO postgres;

--
-- Name: Vol; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Vol" (
    "NV" character varying(6) NOT NULL,
    "NP" integer NOT NULL,
    "NA" integer NOT NULL,
    "VD" character varying(10) NOT NULL,
    "VA" character varying(10) NOT NULL,
    "HD" integer NOT NULL,
    "HA" integer NOT NULL
);


ALTER TABLE public."Vol" OWNER TO postgres;

--
-- Data for Name: Avion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Avion" ("NA", "Nom", "Capacite", "Localite") FROM stdin;
100	AIRBUS	345	RABAT
102	B737	253	RABAT
103	AIRBUS	690	Paris
104	AIRBUS	207	Bruxelles
101	B737	253	CASA
\.


--
-- Data for Name: Pilote; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Pilote" ("NP", "Nom", "Adresse") FROM stdin;
10	WAYNE	39 rue Waynetech, Gotham City
20	AUDITORE	36 rue de Lapomme, Florence
30	LUCIS	15 rue Final Fantasy, Tokyo
\.


--
-- Data for Name: Vol; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Vol" ("NV", "NP", "NA", "VD", "VA", "HD", "HA") FROM stdin;
1	10	100	Paris	Marseille	1000	1100
2	20	104	Rouen	Tour	1000	1300
3	30	102	Lille	Casa	2000	2300
4	10	103	Lille	Paris	2000	2130
5	20	100	Rennes	Toulouse	1500	1730
6	30	101	Rabat	Paris	1430	2130
7	10	102	Paris	Rabat	1000	1700
8	20	104	Toulouse	Rouen	1300	1430
9	30	103	Casa	Toulouse	1730	1830
10	10	103	Tour	Rennes	1600	1730
\.


--
-- Name: Avion pk_avion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Avion"
    ADD CONSTRAINT pk_avion PRIMARY KEY ("NA");


--
-- Name: Pilote pk_pilote; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Pilote"
    ADD CONSTRAINT pk_pilote PRIMARY KEY ("NP");


--
-- Name: Vol pk_vol; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Vol"
    ADD CONSTRAINT pk_vol PRIMARY KEY ("NV");


--
-- Name: Avion existetrigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER existetrigger BEFORE INSERT OR UPDATE ON public."Avion" FOR EACH ROW EXECUTE FUNCTION public.existe();


--
-- Name: Avion modicaptrigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER modicaptrigger BEFORE UPDATE ON public."Avion" FOR EACH ROW EXECUTE FUNCTION public.modifcap();


--
-- Name: Vol fk_vol_avion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Vol"
    ADD CONSTRAINT fk_vol_avion FOREIGN KEY ("NA") REFERENCES public."Avion"("NA");


--
-- Name: Vol fk_vol_pilote; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Vol"
    ADD CONSTRAINT fk_vol_pilote FOREIGN KEY ("NP") REFERENCES public."Pilote"("NP");


--
-- PostgreSQL database dump complete
--

