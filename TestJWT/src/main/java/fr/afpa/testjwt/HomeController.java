package fr.afpa.testjwt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 * Handles requests for the application home page.
 */
@CrossOrigin(value="http://localhost:3000")
@RestController
public class HomeController {
	
	/**
	 * Permet de generer le token lorsque l'authentification est valide
	 * et verifie si la personne qui se connecte est bien un administrateur
	 * @param authentification
	 * @return
	 */
	@PostMapping(value = "/authentification")
	public ResponseEntity authentifPost(@RequestBody Authentification authentification) {
		HttpStatus status;
		String token = null;
		Map<String, String> reponse = new HashMap<String, String>();
		
		//Verification de l'authentification
		boolean verifAuthentification = "Fayak".equals(authentification.getLogin()) && "1234".equals(authentification.getMdp());
		
		if (verifAuthentification) {
			// generation du token (10 zeros car en millisecondes)
			token = Token.createJWT("BackDemoJWT", "FrontDemoJWT", authentification.getLogin(), "admin", 10000000000L);
			reponse.put("token", token);
			status = HttpStatus.OK;
		}
		else {
			status = HttpStatus.BAD_REQUEST;
		}
		
		return ResponseEntity.status(status).body(reponse);
	}
	
	
	@GetMapping(value = "/pageprotected")
	public ResponseEntity getPageProtected(@RequestHeader HttpHeaders header) {
		HttpStatus status;
		Map<String, String> reponse = new HashMap<String, String>();
		
		// Permet de recuperer dans le httpheader la valeur du champ Authorization
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && Token.isAthentificate(token.get(), "admin")) {
				status = HttpStatus.OK;
				reponse.put("message", "Vous �tes authentifi� ! :sourire:");
				return ResponseEntity.status(status).body(reponse);
			}
			
		}
		
		reponse.put("message", "Vous n'�tes pas authentifi� ! :cri:");
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body(reponse);
		
	}
	
	
}
