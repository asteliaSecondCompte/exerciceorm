<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<body>
	
	<!-- Afficher un simple texte -->
	<h3>Afficher un simple texte</h3>
	<c:out value="Test JSTL"/>
	<br/><br/>
	
	
	<!-- Afficher une boucle avec une variable -->
	<h3>Afficher une boucle avec une variable</h3>
	<c:forEach var="i" begin="0" end="10" step="2">
		<c:out value="${ i } N'�coutez pas Astelia !"/>
		<br/>
	</c:forEach>
	<br/>
	
	
	<!-- Afficher un switch -->
	<h3>Afficher un switch</h3>
	<c:set var="toto" value="6"/>
	<c:set var="tutu" value="4"/>
	<c:choose>
		<c:when test="${ toto < tutu  }">
			<c:out value="tutu = "/>
			<c:out value="${ tutu }"/>
		</c:when>
		<c:when test="${ toto >= tutu  }">
			<c:out value="toto = "/>
			<c:out value="${ toto }"/>
		</c:when>
	</c:choose>
	<br/><br/>
	
	
	<!-- Afficher un split avec regex -->
	<h3>Afficher un split avec regex</h3>
	<c:forTokens var="souschaine" items="Une calculatrice, c'est mieux quand �a marche." delims=", ">
		<c:out value="${ souschaine }" /> 
		<br/>
	</c:forTokens>
	<br/>
	
	
	<!-- Afficher les parametres d'une url -->
	<h3>Afficher les parametres d'une url</h3>
	<ul>
		<c:forEach var="aParam" items="${ param }">
			<li>
				un parametre : <c:out value="${ aParam.key }"/> = <c:out value="${ aParam.value }"/>
			</li>
		</c:forEach>
	</ul>
	<br/>
	
	
	<!-- Afficher uniquement le parametre de l'url qui se nomme tech -->
	<!-- Pour tester la suite, ajouter dans l'url : ?param1=value1&param2=value&tech=JSTL -->
	<h3>Afficher uniquement le parametre de l'url qui se nomme tech</h3>
	<ul>
		<c:forEach var="aParam" items="${ param }">
			<c:if test="${ aParam.key == 'tech' }">
				<li>
					un parametre : <c:out value="${ aParam.key }"/> = <c:out value="${ aParam.value }"/>
				</li>
			</c:if>
		</c:forEach>
	</ul>
	<br/>
	
	
	
	<!-- Afficher une liste -->
	<h3>Afficher une liste</h3>
	<a href="STL">Cliquer d'abord ici</a><br/>
	<c:forEach items="${ liste }" var="element" varStatus="status">
		Element <c:out value="${ status.count }"/> -> valeur : <c:out value="${ element }" />
		<br/>
	</c:forEach>
	<br/>
	
</body>
</html>
