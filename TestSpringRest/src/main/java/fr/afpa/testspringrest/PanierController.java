package fr.afpa.testspringrest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.entitespersistees.Panier;
import fr.afpa.repositories.IPanierRepository;

@RestController
public class PanierController {
	
	@Autowired
	private IPanierRepository parnierRepository;
	
	@GetMapping(value = "/panier")
	@ResponseBody
	public List<Panier> listerTypes() {
		return parnierRepository.findAll();
	}
	
	@PutMapping(value = "/panier/modifier")
	@ResponseBody
	public Panier modifierType(@RequestBody Panier panier) {
		return parnierRepository.save(panier);
	}
	
	@PostMapping(value = "/panier/ajouter")
	@ResponseBody
	public Panier ajouterType(@RequestBody Panier panier) {
		return parnierRepository.save(panier);
	}
	
	@DeleteMapping(value = "/panier/supprimer")
	@ResponseBody
	public void supprimerType(@RequestParam(value = "id") int id) {
		parnierRepository.deleteById(id);
	}
	
}
