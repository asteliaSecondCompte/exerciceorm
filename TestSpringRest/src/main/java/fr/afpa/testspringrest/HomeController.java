package fr.afpa.testspringrest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.entitespersistees.Produit;
import fr.afpa.entitespersistees.TypeFruit;
import fr.afpa.repositories.IProduitRepository;
import fr.afpa.repositories.ITypeFruitRepository;

/**
 * Handles requests for the application home page.
 */
@RestController
public class HomeController {
	
	
	@Autowired
	private ITypeFruitRepository typeFruitRepository;
	
	
	@GetMapping(value = "/typeFruit")
	@ResponseBody
	public List<TypeFruit> listerTypes() {
		return typeFruitRepository.findAll();
	}
	
	@PutMapping(value = "/typeFruit/modifier")
	@ResponseBody
	public TypeFruit modifierType(@RequestBody TypeFruit type) {
		return typeFruitRepository.save(type);
	}
	
	@PostMapping(value = "/typeFruit/ajouter")
	@ResponseBody
	public TypeFruit ajouterType(@RequestBody TypeFruit type) {
		return typeFruitRepository.save(type);
	}
	
	@DeleteMapping(value = "/typeFruit/supprimer")
	@ResponseBody
	public void supprimerType(@RequestParam(value = "id") int id) {
		typeFruitRepository.deleteById(id);
	}
	
}