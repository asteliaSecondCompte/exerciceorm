package fr.afpa.testspringrest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.entitespersistees.Produit;
import fr.afpa.repositories.IProduitRepository;

@RestController
public class FruitController {

	@Autowired
	private IProduitRepository produitRepository;
	
	@GetMapping(value = "/fruit")
	@ResponseBody
	public List<Produit> home() {
		return produitRepository.findAll();
	}
	
	/*@GetMapping(value = "/pageAjouter")
	public String allerPageAjouterProduit() {
		return "ajouterProduit";
	}*/
	
	@PutMapping(value = "/fruit/modifier")
	@ResponseBody
	public Produit modifierFruit(@RequestBody Produit produit) {
		return produitRepository.save(produit);
	}
	
	@PostMapping(value = "/fruit/ajouter")
	@ResponseBody
	public Produit ajouterFruit(@RequestBody Produit produit) {
		return produitRepository.save(produit);
	}
	
	@DeleteMapping(value = "/fruit/supprimer")
	@ResponseBody
	public void supprimerFruit(@RequestParam(value = "id") int id) {
		produitRepository.deleteById(id);
	}
	
}
