package fr.afpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitespersistees.TypeFruit;

public interface ITypeFruitRepository extends JpaRepository<TypeFruit, Integer> {

}
