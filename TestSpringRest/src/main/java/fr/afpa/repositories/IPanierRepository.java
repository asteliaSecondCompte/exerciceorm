package fr.afpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitespersistees.Panier;

public interface IPanierRepository extends JpaRepository<Panier, Integer> {

}
