package fr.afpa.entitespersistees;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

@Entity
@SequenceGenerator(name = "typefruit_seq", allocationSize = 1, initialValue = 1)
public class TypeFruit {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "typefruit_seq")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Integer idType;
	private String variete;
	private String couleur;
	
	@OneToMany(mappedBy = "typeFruit", cascade = CascadeType.ALL)
	private List<Produit> listeProduits;
	
	public TypeFruit(String variete, String couleur) {
		this.variete = variete;
		this.couleur = couleur;
	}
	
}
