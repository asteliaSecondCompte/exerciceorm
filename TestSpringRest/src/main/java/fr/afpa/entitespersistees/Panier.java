package fr.afpa.entitespersistees;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString

@SequenceGenerator(name = "panier_seq", initialValue = 1, allocationSize = 1)
@Entity
public class Panier {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "panier_seq")
	private Integer idPanier;
	
	private String libelle;
	
	@OneToMany(mappedBy = "panier", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Produit> listeProduits;
	
	public Panier(String libelle) {
		this.libelle = libelle;
		this.listeProduits = new ArrayList<Produit>();
	}
	
	
}
