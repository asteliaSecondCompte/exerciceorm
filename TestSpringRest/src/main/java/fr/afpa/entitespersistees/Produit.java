package fr.afpa.entitespersistees;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString

@Entity
@SequenceGenerator(name = "produit_seq", allocationSize = 1, initialValue = 1)
public class Produit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "produit_seq")
	private Integer idProduit;
	
	private String libelle;
	
	private Float prix;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "idType")
	private TypeFruit typeFruit;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "idPanier")
	@JsonIgnore
	private Panier panier;
	
	public Produit(String libelle, Float prix) {
		this.libelle = libelle;
		this.prix = prix;
	}
	
}
