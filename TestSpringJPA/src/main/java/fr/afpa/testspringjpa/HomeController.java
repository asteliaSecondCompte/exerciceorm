package fr.afpa.testspringjpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.entitespersistees.Produit;
import fr.afpa.repositories.IProduitRepository;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	@Autowired
	private IProduitRepository produitRepository;
	
	
	@GetMapping(value = "/")
	public ModelAndView home() {
		List<Produit> listeProduits = produitRepository.findAll();
		ModelAndView mv = new ModelAndView();
		mv.addObject("listeProduits", listeProduits);
		mv.setViewName("home");
		return mv;
	}
	
	@GetMapping(value = "/pageAjouter")
	public String allerPageAjouterProduit() {
		return "ajouterProduit";
	}
	
	@PostMapping(value = "/ajouterProduit")
	public String ajouterProduit(@RequestParam(value = "libelle") String libelle, @RequestParam(value = "prix") String prix) {
		Produit produit = new Produit(libelle, Float.parseFloat(prix));
		produitRepository.save(produit);
		return "ajouterProduit";
	}
	
	@GetMapping(value = "/supprimer")
	public ModelAndView supprimerProduit(@RequestParam(value = "id") int id) {
		produitRepository.deleteById(id);
		return home();
	}
	
}