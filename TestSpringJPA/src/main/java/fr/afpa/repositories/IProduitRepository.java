package fr.afpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitespersistees.Produit;


public interface IProduitRepository extends JpaRepository<Produit, Integer> {

}
