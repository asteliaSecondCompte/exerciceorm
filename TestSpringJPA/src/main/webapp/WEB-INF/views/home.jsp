<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Home</title>
</head>
<body>
	<h1>Liste des produits</h1>
	<br />
	
	<div>
	<table>
		<thead>
		<tr>
			<td>Identifiant</td>
			<td>Libelle</td>
			<td>Prix</td>
		</tr>
		</thead>
		<tbody>
			<c:forEach items="${ listeProduits }" var="produit">
				<tr>
					<td><a href="supprimer?id=${ produit.id }"><c:out value="${ produit.id }"/></a></td>
					<td><c:out value="${ produit.libelle }"/></td>
					<td><c:out value="${ produit.prix }"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
	<br/>
	
	<div>
		<a href="pageAjouter">Ajouter un produit</a>
	</div>

</body>
</html>
