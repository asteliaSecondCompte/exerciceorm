package fr.afpa.controles;

public class Controle {

	/**
	 * verifie le format de l'email
	 * 
	 * @param emailSaisi
	 * @return booleen
	 */
	public static boolean formatEmail(String mail) {
		return mail.matches(
				("([a-z]+@[a-z]+\\.[a-z]{2,3})|([a-z]+\\.[a-z]+@[a-z]+\\.[a-z]{2,3})|([a-z]+\\.[a-z]\\.[a-z]+@[a-z]+\\.[a-z]{2,3})"));
	}
	
	/**
	 * verifie qu'on ne saisie pas une chaine vide comme valeur
	 * 
	 * @param nomPrenom
	 * @return booleen
	 */
	public static boolean formatNomPrenom(String nomPrenom) {
		return nomPrenom.equals("[a-zA-Z]+");
	}
	
	/**
	 * Verifie que le numero de telephone saisi est valide
	 * 
	 * @param tel
	 * @return booleen
	 */
	public static boolean formatTelephone(String tel) {
		return tel.equals("[0-9]{10}");
	}
	
}
