package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.controles.Controle;
import fr.afpa.services.ServiceMessage;

/**
 * Servlet implementation class ControleSaisie
 */
public class ControleSaisie extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControleSaisie() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String tel = request.getParameter("tel");
		String mail = request.getParameter("mail");
		if (Controle.formatNomPrenom(nom) && Controle.formatNomPrenom(prenom) 
				&& Controle.formatTelephone(tel) && Controle.formatEmail(mail)) {
			RequestDispatcher dispatch = request.getRequestDispatcher("InfosUser.jsp");
			dispatch.forward(request, response);
		}
		else { 
			ServiceMessage sm = new ServiceMessage();
			String message = sm.message(nom, prenom, tel, mail);
			request.setAttribute("message", message);
			request.setAttribute("nom", nom);
			request.setAttribute("prenom", prenom);
			request.setAttribute("tel", tel);
			request.setAttribute("mail", mail);
			RequestDispatcher dispatch = request.getRequestDispatcher("index.jsp");
			dispatch.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
