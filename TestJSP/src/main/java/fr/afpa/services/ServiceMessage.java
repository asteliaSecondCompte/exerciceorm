package fr.afpa.services;

import fr.afpa.controles.Controle;

public class ServiceMessage {

	public String message(String nom, String prenom, String tel, String mail) {
		String res ="";
		
		if (!Controle.formatNomPrenom(nom)) {
			res += "Le format du nom n'est pas respecté !<br/>";
		}
		
		if (!Controle.formatNomPrenom(prenom)) {
			res += "Le format du prénom n'est pas respecté !<br/>";
		}
		
		if (!Controle.formatTelephone(tel)) {
			res += "Le format du numéro de téléphone n'est pas respecté !<br/>";
		}
		
		if (!Controle.formatEmail(mail)) {
			res += "Le format de l'email n'est pas respecté !<br/>";
		}
		
		return res;
	}
	
}
